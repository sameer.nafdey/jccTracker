var selectedItemType;
var selectedItemId;
var currentPanel = 'panelItemManagement';

function displayPanel(name){
    jQuery('#'+currentPanel).hide();
    jQuery('#'+name).show();
    currentPanel = name;
    
    if(name==='panelItemManagement'){
        jQuery('#btnItmMgmt').addClass('active');
        jQuery('#btnUsrMgmt').removeClass('active');
    }else{
        jQuery('#btnItmMgmt').removeClass('active');
        jQuery('#btnUsrMgmt').addClass('active');
    }
}

//function displayAllUsers(){
//    var request = new Object();
//    request['id'] = 'all';
//    jQuery.get('/admin/user/get/all',request ,function (data) {
//        
////        console.log("Success Response:"+data);
//        if(data){
//            jQuery('#userListPlaceholder').html(data);
//        }
//        clearItemForm();
//    }).fail(function (e) {
//        console.log('error'+e);
//    }).always(function(){
//        jccTrackerApp.hidePleaseWait();
//    });
//}

function selectItemType(val){
    selectedItemType = val;
    jQuery('#btnDropDownItemType').html(val.capitalizeFirstLetter()+' <span class="caret"></span>');
    getItemDefinitionByType();
}

function getItemDefinitionByType() {
    var queryObj = new Object();
    queryObj.entity_type = selectedItemType;
    jccTrackerApp.showPleaseWait();
    jQuery.get('/admin/item/definition/get', queryObj, function (data) {
        
//        console.log("Success Response:"+data);
        if(data){
            jQuery('#defListPlaceholder').html(data);
        }
        
    }).fail(function (e) {
        console.log('error'+e);
    }).always(function(){
        jccTrackerApp.hidePleaseWait();
    });
}

function saveItemDef(){
    if(!validateItemForm()){
        return false;
    }
    var saveObject = new Object();
    saveObject['entity_type']=selectedItemType;
    saveObject['title'] = jQuery('#entityItemTitle').val();
    saveObject['desc'] = jQuery('#entityItemDesc').val();
    
    jQuery.post('/admin/item/definition/store', saveObject, function (data) {
        
//        console.log("Success Response:"+data);
        if(data){
            jQuery('#defListPlaceholder').html(data);
        }
        clearItemForm();
    }).fail(function (e) {
        console.log('error'+e);
    }).always(function(){
        jccTrackerApp.hidePleaseWait();
    });
}

function removeItemDefinition(id){
    var deleteObject = new Object();
    deleteObject['id'] = id;
    deleteObject['entity_type'] = selectedItemType;
    jQuery.post('/admin/item/definition/remove', deleteObject, function (data) {
        
        if(data){
            jQuery('#defListPlaceholder').html(data);
        }
        
    }).fail(function (e) {
        console.log('error'+e);
    }).always(function(){
        jccTrackerApp.hidePleaseWait();
    });
}

function editItemDefinition(id, title, desc){
    jQuery("#editButtons").show();
    jQuery('#btnEntityItemDefSave').hide();
    jQuery("#entityItemTitle").val(title);
    jQuery('#entityItemDesc').val(desc);
    selectedItemId=id;
}

function cancelEdit(){
    jQuery("#editButtons").hide();
    jQuery('#btnEntityItemDefSave').show();
    jQuery("#entityItemTitle").val('');
    jQuery('#entityItemDesc').val('');
    selectedItemId='';
}

function updateItemDefinition(){
    jQuery('#editButtons').hide();
    jQuery('#btnEntityItemDefSave').show();
    if(!validateItemForm()){
        return false;
    }
    var updateObject = new Object();
    updateObject['id'] = selectedItemId;
    updateObject['entity_type'] = selectedItemType;
    updateObject['title'] = jQuery("#entityItemTitle").val();
    updateObject['desc'] = jQuery('#entityItemDesc').val();
    jQuery.post('/admin/item/definition/update', updateObject, function (data) {
        
        if(data){
            jQuery('#defListPlaceholder').html(data);
        }
        clearItemForm();
    }).fail(function (e) {
        console.log('error'+e);
    }).always(function(){
        jccTrackerApp.hidePleaseWait();
    });
}

function clearItemForm(){
    jQuery("#entityItemTitle").val('');
    jQuery('#entityItemDesc').val('');
}

function validateItemForm(){
    if(jQuery("#entityItemTitle").val().length>0 && jQuery('#entityItemDesc').val().length>0 && selectedItemType){
        jQuery('#warningAlert').hide();
        return true;
    }else{
        jQuery('#warningAlert').show();
        return false;
    }
    
}