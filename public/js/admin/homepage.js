
var flats = [];
var selectedRecord = new Object();
var blocks = [];

function initializeArrays() {
    if (flats.length === 0) {
        jQuery.each(BLOCK_FLAT, function (key, value) {
            if (flats[value['block_id']] !== undefined) {
                flats[value['block_id']].push(value['unit']);
            } else {
                flats[value['block_id']] = [value['unit']];
            }
        });
    }
    if (blocks.length === 0) {
        jQuery.each(BLOCKS, function (key, value) {
            blocks[value['id']] = value['name'];
        });
    }
}

function listFlatsForBlock(id) {
    return flats[id.toString()];
}

function selectBlock(blockid) {
    initializeArrays();
    selectedRecord.blockId = blockid;
    showFloors();
    showSelected();
}

function showFloors() {
    //clear floors
    jQuery('#select-floors .list-group').empty();
    for (var i = 1; i <= 13; i++) {

        jQuery('#select-floors .list-group').append(
                jQuery('<a>').addClass('list-group-item').attr('href', '#').attr('onclick', 'selectFloor("' + i + '");return false;').append(i.toString() + '<sup>' + getOrdinal(i) + '</sup> Floor')
                );
    }
    //hide blocks
    jQuery("#select-common-blocks").hide();

    //show floors
    jQuery('#select-floors').show();
}

function selectFloor(floorNumber) {
    selectedRecord.floor = floorNumber;
    showFlatsByBlock(selectedRecord.blockId);
    showSelected();
}

function showFlatsByBlock(blockid) {
    //clear floorsø
    jQuery('#select-flats .list-group').empty();

    var flts = listFlatsForBlock(blockid);

    for (var i = 0; i < flts.length; i++) {

        jQuery('#select-flats .list-group').append(
                jQuery('<a>').addClass('list-group-item').attr('href', '#').attr('onclick', 'selectFlat("' + flts[i] + '");return false;').append(flts[i].toString() + '<sup>' + getOrdinal(flts[i]) + '</sup> Unit')
                );
    }
    //hide blocks
    jQuery("#select-floors").hide();

    //show floors
    jQuery('#select-flats').show();
}

function selectFlat(flatNumber) {
    selectedRecord.flatNumber = flatNumber;
    //console.log(selectedRecord);
    showSelected();
    jQuery('#select-flats').hide();
    getFlatID();
}

function showSelected() {
    var lbl = "";
    if (selectedRecord.blockId) {
        lbl = blocks[selectedRecord.blockId];
    }
    if (selectedRecord.floor) {
        lbl = lbl + "-" + selectedRecord.floor;
    } else {
        lbl = lbl + " Block";
    }
    if (selectedRecord.flatNumber) {
        lbl = lbl + (selectedRecord.flatNumber < 10 ? "0" + selectedRecord.flatNumber : selectedRecord.flatNumber);
    } else if (selectedRecord.floor) {
        lbl = lbl + '<sup>' + getOrdinal(selectedRecord.floor) + "</sup> Floor";
    }
    jQuery('#displayFlatName').empty().append(lbl);
    jQuery('#reset-selected-record').show();
}

function resetFlatSelection() {
    selectedRecord = new Object();
    jQuery('#select-flats').hide();
    jQuery('#select-floors').hide();
    jQuery("#select-common-blocks").show();
    jQuery('#displayFlatName').empty().append('Please select below.');
    jQuery('#reset-selected-record').hide();
}

function validateForm(){
    var validity =  isFlatValid();
    if(!validity){
        jQuery('#incomplete-flat-error').show();
        jQuery('html,body').animate({ scrollTop: jQuery('#incomplete-flat-error').offset().top}, 1000);
        return false;
    }else{
        jQuery('#incomplete-flat-error').hide();
        if(jQuery('#flatid').val())
            return true;
        else
            return false;
    }
    
    
    return validity;
}

function isFlatValid(){
    if(selectedRecord){
        if(!selectedRecord.blockId && !selectedRecord.floor && !selectedRecord.flatNumber)
            return true;
        else if(selectedRecord.blockId && !selectedRecord.floor)
            return false;
        else if(selectedRecord.blockId && selectedRecord.floor && !selectedRecord.flatNumber)
            return false;
        else if(selectedRecord.blockId && selectedRecord.floor && selectedRecord.flatNumber)
            return true;
    }
    return false;
}

function getFlatID() {
    
    jccTrackerApp.showPleaseWait();
    jQuery.get('/flatid', selectedRecord, function (data) {
        
//        console.log("Success Response:"+data);
        if(data){
            if(data['owner-exist']===true){
                alert('As per records "'+jQuery('#displayFlatName').html()+'" belongs to '+data['owner-name']);
                resetFlatSelection();
            }else{
                jQuery('#flatid').val(data['flatid']);
            }
        }
        
    }).fail(function (e) {
        console.log('error'+e);
    }).always(function(){
        jccTrackerApp.hidePleaseWait();
    });
}