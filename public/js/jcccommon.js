function getOrdinal(n) {
    var s = ["th", "st", "nd", "rd"],
            v = n % 100;
    return (s[(v - 20) % 10] || s[v] || s[0]);
}

String.prototype.capitalizeFirstLetter = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}


// CSRF protection
jQuery.ajaxSetup(
        {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }
);


function toastSuccess() {
    jQuery.toast({
        text: "Thank you! Appreciate your efforts.", // Text that is to be shown in the toast
        heading: 'Done', // Optional heading to be shown on the toast
        icon: 'success', // Type of toast icon
        showHideTransition: 'fade', // fade, slide or plain
        allowToastClose: true, // Boolean value true or false
        hideAfter: 3000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
        position: 'bottom-center', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values



        textAlign: 'left', // Text alignment i.e. left, right or center
        loader: false, // Whether to show loader or not. True by default
        loaderBg: '#9EC600', // Background color of the toast loader
        beforeShow: function () {
        }, // will be triggered before the toast is shown
        afterShown: function () {
        }, // will be triggered after the toat has been shown
        beforeHide: function () {
        }, // will be triggered before the toast gets hidden
        afterHidden: function () {
        }  // will be triggered after the toast has been hidden
    });
}

function toastError(customMessage) {
    jQuery.toast({
        text: "Something went wrong." + customMessage, // Text that is to be shown in the toast
        heading: 'Duh!', // Optional heading to be shown on the toast
        icon: 'error', // Type of toast icon
        showHideTransition: 'fade', // fade, slide or plain
        allowToastClose: true, // Boolean value true or false
        hideAfter: 3000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
        position: 'bottom-center', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values



        textAlign: 'left', // Text alignment i.e. left, right or center
        loader: false, // Whether to show loader or not. True by default
        loaderBg: '#9EC600', // Background color of the toast loader
        beforeShow: function () {
        }, // will be triggered before the toast is shown
        afterShown: function () {
        }, // will be triggered after the toat has been shown
        beforeHide: function () {
        }, // will be triggered before the toast gets hidden
        afterHidden: function () {
        }  // will be triggered after the toast has been hidden
    });
}

var jccTrackerApp;
jccTrackerApp = jccTrackerApp || (function () {
    var pleaseWaitDiv = $('<div id="myModal" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Please wait..</h4></div><div class="modal-body"><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div></div></div></div></div>');

    return {
        showPleaseWait: function () {
            pleaseWaitDiv.modal('show');
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },
    };
})();