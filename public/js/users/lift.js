function pullLiftsForBlock(blockId, blockName){
    jQuery('#btn-block-select').html(blockName + ' Block <span class="caret"></span>');
    jQuery('#selectedEntityTitle').html(blockName + ' Block');
    
    jccTrackerApp.showPleaseWait();
    jQuery.get('/api/uiview/get/lift/'+blockId, [], function (data) {
        
        if(data){
            jQuery('#div-lift-selection').html(data);
            jQuery('#div-lift-selection').show();
        }
        
    }).fail(function (e) {
        console.log('error'+e);
    }).always(function(){
        jccTrackerApp.hidePleaseWait();
        $('.my-tooltip').tooltip('show');
    });
    jQuery('#panelBodyForTrackingItem').hide();
    jQuery('#panelBodyNoTrackingItem').show();
    
}

function selectLiftForUpdate(blockId, liftNumber){

    jQuery('#btn-lift-select').html('Lift '+liftNumber+' <span class="caret"></span>');
    window.location.href = '/home/lift/'+blockId+'/'+liftNumber;
}

jQuery(document).ready(function () {
    $('.my-dropdown').dropdown();
    $('.my-tooltip').tooltip();

});