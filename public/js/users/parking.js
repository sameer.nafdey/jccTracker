function pullCorridorsForBlock(blockId, blockName){
    jQuery('#btn-block-select').html(blockName + ' Block <span class="caret"></span>');
    jQuery('#selectedEntityTitle').html(blockName + ' Block');
    
    jccTrackerApp.showPleaseWait();
    jQuery.get('/api/uiview/get/parking/'+blockId, [], function (data) {
        
        if(data){
            jQuery('#div-corridor-selection').html(data);
            jQuery('#div-corridor-selection').show();
        }
        
    }).fail(function (e) {
        console.log('error'+e);
    }).always(function(){
        jccTrackerApp.hidePleaseWait();
        $('.my-tooltip').tooltip('show');
    });
    jQuery('#panelBodyForTrackingItem').hide();
    jQuery('#panelBodyNoTrackingItem').show();
    
}

function selectCorridorForUpdate(blockId, floorNumber){

    jQuery('#btn-corridor-select').html('Floor '+floorNumber+' <span class="caret"></span>');
    window.location.href = '/home/parking/'+blockId+'/'+floorNumber;
}

jQuery(document).ready(function () {
    $('.my-dropdown').dropdown();
    $('.my-tooltip').tooltip();

});