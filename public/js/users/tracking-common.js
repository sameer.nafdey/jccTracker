statusArray = [];
function setStatus(defId, entity_id, status) {
    statusArray[''+defId] = {"defId": defId, "entity_id": entity_id, "status": status};
    ;
    jQuery('#btn-item-' + defId).html(statusLabel(status) + ' <span class="caret"></span>');
    jQuery('#btn-item-' + defId).removeClass('btn-default');
    jQuery('#btn-item-' + defId).addClass('btn-primary');

}

function statusLabel(status) {
    if (status === 'NOTSTARTED') {
        return 'Not started';
    } else if (status === 'INPROGRESS') {
        return 'In progress';
    } else if (status === 'COMPLETED') {
        return 'Completed';
    }
}

function save(routeName, param1, param2, param3) {
    
    if (statusArray.length <= 0) {
        alert('Are you sure nothing to save?');
        return;
    }
    
    var requestObject = new Object();
    requestObject['data'] = statusArray;
    if(param1)
        requestObject['param1'] = param1;
    if(param2)
        requestObject['param2'] = param2;
    if(param3)
        requestObject['param3'] = param3;
    
    jccTrackerApp.showPleaseWait();
    jQuery.post(routeName, requestObject, function (data) {

        
        if (data) {
            window.location.href = data;
        }

    }).fail(function (e) {
        console.log('error' + e);
    }).always(function () {
        jccTrackerApp.hidePleaseWait();
    });
}

jQuery(document).ready(function () {
    if (completionPercentage && completionPercentage>0) {
        
        jQuery('.progress').show();
        jQuery('.progress-bar').width(completionPercentage + '%');
        jQuery('.progress-bar').html(completionPercentage + '% Completed');
    }else{
        jQuery('.progress').hide();
    }

});