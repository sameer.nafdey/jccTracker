var flats = [];
var blocks = [];

var selectedItemDefID = '';
var selectedBlocksStatus = new Array();

function initializeArrays() {

    if (flats.length === 0) {
        jQuery.each(BLOCK_FLAT, function (key, value) {
            if (flats[value['block_id']] !== undefined) {
                flats[value['block_id']].push(value['unit']);
            } else {
                flats[value['block_id']] = [value['unit']];
            }
        });
    }
    if (blocks.length === 0) {
        jQuery.each(BLOCKS, function (key, value) {
            blocks[value['id']] = value['name'];
        });
    }
}

function selectItem(itemDefId, title) {

    selectedItemDefID = itemDefId;

    jQuery('#itemLabel').html(title);
    jQuery('#select-item-div').hide();


    if (selectedItemDefID) {
        var getCorridorWithStatusRequest = new Object();
        getCorridorWithStatusRequest.itemDefId = selectedItemDefID;
        getCorridorWithStatusRequest.item_type = 'block';

        jQuery.get('/combined-updates/blocks/getstatus/', getCorridorWithStatusRequest, function (data) {

            if (data) {
                jQuery('#trackingPanel').html(data);
                jQuery('#trackingPanel').show();
                updateProgressBar();
            }

        }).fail(function (e) {
            console.log('error' + e);
        }).always(function () {
            jccTrackerApp.hidePleaseWait();
        });
    }

}

function reset() {

    selectedItemDefID = '';


    selectedBlocksStatus = new Array();
    jQuery('#mainPanel').show();
    jQuery('#select-item-div').show();
    jQuery('#trackingPanel').hide();
    jQuery('#trackingPanel').html('');


    jQuery(':checked').parent().removeClass('active');
    jQuery(':checked').prop('checked', false);

}

function save() {

    var blockObj;
    jQuery(':checked').each(function(){
        blockObj = new Object();
        blockObj.blockid = jQuery(this).data('value');
        blockObj.status = jQuery(this).val();
        selectedBlocksStatus.push(blockObj);
    });
    
    var saveRequest = new Object();
    saveRequest.blockList = selectedBlocksStatus;
    saveRequest.itemDefId = selectedItemDefID;
    
    console.log(saveRequest);
    
    jQuery.post('/track/combined/blocks', saveRequest, function (data) {

        
        if (data) {
            alert(data['success']);
            reset();
        }

    }).fail(function (e) {
        console.log('error' + e);
    }).always(function () {
        jccTrackerApp.hidePleaseWait();
    });
}



jQuery(document).ready(function () {
    initializeArrays();
});

function updateProgressBar() {
    if (completionPercentage && completionPercentage > 0) {

        jQuery('.progress').show();
        jQuery('.progress-bar').width(completionPercentage + '%');
        jQuery('.progress-bar').html(completionPercentage + '% Completed');
    } else {

        jQuery('.progress').hide();
    }
}