var flats = [];
var blocks = [];

var selectedBlockID = '';
var selectedFloorNumber = '';
var selectedItemDefID = '';
var currentShownFlatDiv = '';
var selectedFlats = new Array();

function initializeArrays() {

    if (flats.length === 0) {
        jQuery.each(BLOCK_FLAT, function (key, value) {
            if (flats[value['block_id']] !== undefined) {
                flats[value['block_id']].push(value['unit']);
            } else {
                flats[value['block_id']] = [value['unit']];
            }
        });
    }
    if (blocks.length === 0) {
        jQuery.each(BLOCKS, function (key, value) {
            blocks[value['id']] = value['name'];
        });
    }
}

function selectBlock(id) {

    selectedBlockID = id;
    selectedFloorNumber = '';
    selectedItemDefID = '';
    jQuery('#btn-block-select').html(blocks[id] + ' Block <span class="caret"></span>');
    jQuery('#btn-floor-select').html('Select Floor <span class="caret"></span>');
    
    if (currentShownFlatDiv) {
        jQuery('#' + currentShownFlatDiv).hide();
        jQuery('#trackingPanel').hide();
    }
}

function selectFloor(floorNumber) {
    selectedItemDefID = '';
    
    if (!selectedBlockID) {
        alert('Select a Block first.');
        return false;
    } else if (floorNumber) {
        selectedFloorNumber = floorNumber;
        jQuery('#btn-floor-select').html(selectedFloorNumber + '<sup>' + getOrdinal(selectedFloorNumber) + '</sup> Floor <span class="caret"></span>');
        
        jQuery('#select-item-div').show();
        return true;
    }
    if (currentShownFlatDiv) {
        jQuery('#' + currentShownFlatDiv).hide();
        jQuery('#trackingPanel').hide();
        
    }
}

function selectItem(itemDefId, title) {
    if (selectedFloorNumber && selectedBlockID) {
        selectedItemDefID = itemDefId;
        
        jQuery('#itemLabel').html(title);
        jQuery('#select-item-div').hide();
        
        if (selectedBlockID && selectedItemDefID) {
            var getFlatsWithStatusRequest = new Object();
            getFlatsWithStatusRequest.blockId = selectedBlockID;
            getFlatsWithStatusRequest.itemDefId = selectedItemDefID;
            getFlatsWithStatusRequest.item_type = 'flat';
            getFlatsWithStatusRequest.floor = selectedFloorNumber;

            jQuery.get('/combined-updates/flats/getstatus/', getFlatsWithStatusRequest, function (data) {

               if(data){
                   jQuery('#trackingPanel').html(data);
                   jQuery('#trackingPanel').show();
                   updateProgressBar();
               }

            }).fail(function (e) {
                console.log('error' + e);
            }).always(function () {
                jccTrackerApp.hidePleaseWait();
            });
        }
        
    }
}

jQuery(document).ready(function() {
    initializeArrays();
});

function reset(){
    jQuery('#' + currentShownFlatDiv).hide();
    selectedBlockID = '';
    selectedFloorNumber = '';
    selectedItemDefID = '';
    currentShownFlatDiv = '';
    selectedFlats = new Array();
    jQuery('#mainPanel').show();
    jQuery('#trackingPanel').hide();
    
    jQuery('#btn-block-select').html('Select Block <span class="caret"></span>');
    jQuery('#btn-floor-select').html('Select Floor <span class="caret"></span>');
        
}

function save(){
    var flatObj;
    jQuery(':checked').each(function(){
        flatObj = new Object();
        flatObj.flatid = jQuery(this).data('value');
        flatObj.status = jQuery(this).val();
        selectedFlats.push(flatObj);
    });
    
    var saveRequest = new Object();
    saveRequest.blockId = selectedBlockID;
    saveRequest.flatsList = selectedFlats;
    saveRequest.itemDefId = selectedItemDefID;
    
    jQuery.post('/track/combined/flats', saveRequest, function (data) {

        
        if (data) {
            alert(data['success']);
            reset();
        }

    }).fail(function (e) {
        console.log('error' + e);
    }).always(function () {
        jccTrackerApp.hidePleaseWait();
    });
}

function updateProgressBar(){
    if (completionPercentage && completionPercentage>0) {
        
        jQuery('.progress').show();
        jQuery('.progress-bar').width(completionPercentage + '%');
        jQuery('.progress-bar').html(completionPercentage + '% Completed');
    }else{
        jQuery('.progress').hide();
    }
}