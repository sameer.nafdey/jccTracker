var flats = [];
var blocks = [];

var selectedBlockID = '';
var selectedItemDefID = '';
var selectedParkingStatus = new Array();

function initializeArrays() {

    if (flats.length === 0) {
        jQuery.each(BLOCK_FLAT, function (key, value) {
            if (flats[value['block_id']] !== undefined) {
                flats[value['block_id']].push(value['unit']);
            } else {
                flats[value['block_id']] = [value['unit']];
            }
        });
    }
    if (blocks.length === 0) {
        jQuery.each(BLOCKS, function (key, value) {
            blocks[value['id']] = value['name'];
        });
    }
}

function selectBlock(id) {

    selectedBlockID = id;
    if (selectedBlockID) {
        jQuery('#select-item-div').show();
        jQuery('#trackingPanel').hide();
        jQuery('#trackingPanel').html();
    }
    selectedItemDefID = '';

    jQuery('#btn-block-select').html(blocks[id] + ' Block <span class="caret"></span>');

}

function selectItem(itemDefId, title) {
    if (selectedBlockID) {
        selectedItemDefID = itemDefId;

        jQuery('#itemLabel').html(title);
        jQuery('#select-item-div').hide();
        

        if (selectedBlockID && selectedItemDefID) {
            var getParkingWithStatusRequest = new Object();
            getParkingWithStatusRequest.blockId = selectedBlockID;
            getParkingWithStatusRequest.itemDefId = selectedItemDefID;
            getParkingWithStatusRequest.item_type = 'parking';

            jQuery.get('/combined-updates/parkings/getstatus/', getParkingWithStatusRequest, function (data) {

               if(data){
                   jQuery('#trackingPanel').html(data);
                   jQuery('#trackingPanel').show();
                   updateProgressBar();
               }

            }).fail(function (e) {
                console.log('error' + e);
            }).always(function () {
                jccTrackerApp.hidePleaseWait();
            });
        }
    }
}

function reset() {

    selectedBlockID = '';

    selectedItemDefID = '';


    selectedParkingStatus = new Array();
    jQuery('#mainPanel').show();
    jQuery('#trackingPanel').hide();

    jQuery('#btn-block-select').html('Select Block <span class="caret"></span>');


    jQuery(':checked').parent().removeClass('active');
    jQuery(':checked').prop('checked', false);

}

function save() {

    var parkingObj;
    jQuery(':checked').each(function(){
        parkingObj = new Object();
        parkingObj.parkingid = jQuery(this).data('value');
        parkingObj.status = jQuery(this).val();
        selectedParkingStatus.push(parkingObj);
    });
    


    var saveRequest = new Object();
    saveRequest.blockId = selectedBlockID;

    saveRequest.parkingList = selectedParkingStatus;
    saveRequest.itemDefId = selectedItemDefID;
    
    jQuery.post('/track/combined/parkings', saveRequest, function (data) {


        if (data['success']) {
            alert(data['success']);
            reset();
        }

    }).fail(function (e) {
        console.log('error' + e);
    }).always(function () {
        jccTrackerApp.hidePleaseWait();
    });
}



jQuery(document).ready(function () {
    initializeArrays();
});

function updateProgressBar(){
    if (completionPercentage && completionPercentage>0) {
        
        jQuery('.progress').show();
        jQuery('.progress-bar').width(completionPercentage + '%');
        jQuery('.progress-bar').html(completionPercentage + '% Completed');
    }else{
        
        jQuery('.progress').hide();
    }
}