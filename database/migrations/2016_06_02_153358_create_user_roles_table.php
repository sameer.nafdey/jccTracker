<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //no more required as we are now using entrust package.
//        Schema::create('user_roles', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id')->nullable(false);
//            $table->enum('role', ['admin', 'super-user', 'customer','jains-admin','jains-employee'])->nullable(false);
//            $table->integer('created_by')->nullable(false);
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('user_roles');
    }
}
