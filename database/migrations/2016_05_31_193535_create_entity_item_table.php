<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_id'); //this may contain flat id, block id, corridor id, lift id or even parking id
            $table->enum('entity_type', ['BLOCK', 'FLAT', 'CORRIDOR','LIFT','PARKING']);
            $table->integer('item_def_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('item_def_id')->references('id')->on('entity_item_definitions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entity_items');
    }
}
