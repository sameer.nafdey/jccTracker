<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('user_profiles', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('user_id')->unsigned();
    		
    		$table->string('contact_no')->nullable();
    		$table->string('alternate_contact_no')->nullable();
    		$table->string('alternate_email_id')->nullable();
    		$table->boolean('is_flat_owner');
    		$table->boolean('availability_in_city')->default(true); 
    		$table->boolean('is_builder_employee')->default(false);
    		$table->string('fb_url')->nullable();
    		$table->string('twitter_url')->nullable();
    		$table->string('avatar_url')->nullable();
    		//we will later have transaction table to record association fee payment w.r.t. flats
    		
    		$table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
    		
    		
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drop('user_profiles');
    }
}
