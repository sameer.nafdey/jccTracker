<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityItemStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('entity_item_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_item_id')->unsigned();
            $table->enum('status', ['COMPLETED', 'INPROGRESS', 'NOTSTARTED', 'NOTAPPLICABLE']);
            $table->dateTime('recorded_at');
            $table->integer('recorded_by')->unsigned();
            
            $table->foreign('entity_item_id')->references('id')->on('entity_items');
            $table->foreign('recorded_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entity_item_statuses');
    }
}
