<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityItemDefinitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_item_definitions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('entity_type', ['block', 'flat', 'corridor','lift','parking']);
            $table->string('title', 100);
            $table->string('description', 250)->nullable();
            $table->integer('created_by')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entity_item_definitions');
    }
}
