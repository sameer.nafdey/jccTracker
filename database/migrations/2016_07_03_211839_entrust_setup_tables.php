<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });

        // Create table for storing permissions
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });
    }
    
    private function initializePermissions(){
        $permission = new App\Model\Permission();
        $permission->name = 'combined-updates-read';
        $permission->display_name = 'View combined updates';
        $permission->description = 'Permission to View combined updates of flats/blocks/corridors/parkings/lifts.';
        $permission->save();

        $permission = new App\Model\Permission();
        $permission->name = 'combined-updates-rw';
        $permission->display_name = 'Post combined updates';
        $permission->description = 'Permission to post combined updates at a time.';
        $permission->save();

        $permission = new App\Model\Permission();
        $permission->name = 'admin-user-read';
        $permission->display_name = 'Admin user read-only';
        $permission->description = 'Read only permission for administration data of user';
        $permission->save();

        $permission = new App\Model\Permission();
        $permission->name = 'admin-user-rw';
        $permission->display_name = 'Admin user read-write';
        $permission->description = 'Permission to add new or modify existing users details.';
        $permission->save();



        $permission = new App\Model\Permission();
        $permission->name = 'admin-role-manage';
        $permission->display_name = 'Admin role manage';
        $permission->description = 'Permission to manage roles i.e. define/update/delete.';
        $permission->save();

        $permission = new App\Model\Permission();
        $permission->name = 'admin-role-assign';
        $permission->display_name = 'Admin role assign';
        $permission->description = 'Permission to be able to assign roles to users';
        $permission->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('permission_role');
        Schema::drop('permissions');
        Schema::drop('role_user');
        Schema::drop('roles');
    }
}
