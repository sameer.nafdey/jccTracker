<?php

use Illuminate\Database\Seeder;
use App\Model\EntityItemDefinition;
use App\User;

class EntityItemDefinitionTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $superman = User::where('name', 'Superman')->first();

        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Exterior Plastering', 'DESCRIPTION' => 'Plastering on outside surface of block', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Front Stairs Granite', 'DESCRIPTION' => 'Granite work from basement upto 13th floor for front side staircase', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Back Stairs Granite', 'DESCRIPTION' => 'Granite work from basement upto 13th floor for second staircase', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Terrace waterproofing', 'DESCRIPTION' => 'Waterproofing of terrace for block', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Water tanks', 'DESCRIPTION' => 'Completion of water tank for storage and supply', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Sewerage', 'DESCRIPTION' => 'All flat drainage connected with free flow', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Rain protection sheets', 'DESCRIPTION' => 'Sheets installed for protection from rain water between flats', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'block', 'TITLE' => 'Diesel Generator', 'DESCRIPTION' => 'Installation', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'corridor', 'TITLE' => 'Granite', 'DESCRIPTION' => 'Lobby granite work', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'corridor', 'TITLE' => 'Wall putty first hand', 'DESCRIPTION' => 'First instalment of wall putty', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'corridor', 'TITLE' => 'Electrical wiring', 'DESCRIPTION' => 'Wiring completion', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'corridor', 'TITLE' => 'Duct closure', 'DESCRIPTION' => 'Ducts are closed with panels installed', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'corridor', 'TITLE' => 'Lighting', 'DESCRIPTION' => 'Installation of light bulbs in corridor', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'corridor', 'TITLE' => 'Grill painting', 'DESCRIPTION' => 'Painting on grills ', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Plastering', 'DESCRIPTION' => 'Internal flat plastering', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Tiling', 'DESCRIPTION' => 'Tiling work inside flat', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Wall putty', 'DESCRIPTION' => 'Wall putty inside flat', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Electrical wiring', 'DESCRIPTION' => 'Laying wires inside flat', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Electrical switches', 'DESCRIPTION' => 'Electrical switch boards installed and functioning', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Windows', 'DESCRIPTION' => 'Window frames installed and not broken', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Doors', 'DESCRIPTION' => 'All wooden doors without damages', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Mosquito mesh throughout', 'DESCRIPTION' => 'Mosquito mesh installed throughout flat', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Plumbing fitting', 'DESCRIPTION' => 'Fitting of faucets etc', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Registration', 'DESCRIPTION' => 'Registration of flat for ownership', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Balcony grill pain', 'DESCRIPTION' => 'Painting on grills of balcony in flat', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'Electric meter', 'DESCRIPTION' => 'Installation of electric meters', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'flat', 'TITLE' => 'DG Backup', 'DESCRIPTION' => 'Availability of DG power supply', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'lift', 'TITLE' => 'Installation', 'DESCRIPTION' => 'Lift installation completion', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'lift', 'TITLE' => 'Functioning', 'DESCRIPTION' => 'Lift is functioning', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'lift', 'TITLE' => 'DG Supply', 'DESCRIPTION' => 'Lift is connected with DG Supply', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'parking', 'TITLE' => 'Garbage cleanup', 'DESCRIPTION' => 'Garbage is cleaned', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'parking', 'TITLE' => 'Wall putty/painting', 'DESCRIPTION' => 'Wall putty or painting work', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'parking', 'TITLE' => 'Floor marking', 'DESCRIPTION' => 'Marking of parking blocks on floors', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'parking', 'TITLE' => 'Driveway marking', 'DESCRIPTION' => 'Marking for driveway in/out of parking area', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'parking', 'TITLE' => 'Electrical wiring', 'DESCRIPTION' => 'Wiring installation in parking area', 'CREATED_BY' => $superman->id]);
        EntityItemDefinition::create(['ENTITY_TYPE' => 'parking', 'TITLE' => 'Lighting arrangement', 'DESCRIPTION' => 'Installation of light bulbs', 'CREATED_BY' => $superman->id]);
    }

}
