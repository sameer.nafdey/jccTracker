<?php


use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->command->info('User table seeded!');
        $this->call(PermissionsTableSeeder::class);
        $this->command->info('Permission table seeded!');
        
        $this->call(StaticDataTableSeeder::class);
        $this->command->info('Static data seeded!');
        
        $this->call(EntityItemDefinitionTableSeeder::class);
        $this->command->info('Entity item definitions seeded!');
    }
}
