<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    
    var $superman_role;
    var $admin_role;
    var $flat_owner;
    
    function __construct() {
        $this->superman_role = App\Model\Role::where('name', 'superman')->first();
        $this->admin_role = App\Model\Role::where('name', 'admin')->first();
        $this->flat_owner = App\Model\Role::where('name', 'flatowner')->first();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $permission = new App\Model\Permission();
        $permission->name = 'combined-updates-read';
        $permission->display_name = 'View combined updates';
        $permission->description = 'Permission to View combined updates of flats/blocks/corridors/parkings/lifts.';
        $permission->save();
        
        $this->attachToSuperMan($permission);
        $this->attachToAdmin($permission);
        
        $permission = new App\Model\Permission();
        $permission->name = 'combined-updates-rw';
        $permission->display_name = 'Post combined updates';
        $permission->description = 'Permission to post combined updates at a time.';
        $permission->save();
        
        $this->attachToSuperMan($permission);
        $this->attachToAdmin($permission);

        $permission = new App\Model\Permission();
        $permission->name = 'admin-user-read';
        $permission->display_name = 'Admin user read-only';
        $permission->description = 'Read only permission for administration data of user';
        $permission->save();
        
        $this->attachToSuperMan($permission);
        $this->attachToAdmin($permission);

        $permission = new App\Model\Permission();
        $permission->name = 'admin-user-rw';
        $permission->display_name = 'Admin user read-write';
        $permission->description = 'Permission to add new or modify existing users details.';
        $permission->save();
        
        $this->attachToSuperMan($permission);
        $this->attachToAdmin($permission);

        $permission = new App\Model\Permission();
        $permission->name = 'admin-role-manage';
        $permission->display_name = 'Admin role manage';
        $permission->description = 'Permission to manage roles i.e. define/update/delete.';
        $permission->save();
        
        $this->attachToSuperMan($permission);
        $this->attachToAdmin($permission);

        $permission = new App\Model\Permission();
        $permission->name = 'admin-role-assign';
        $permission->display_name = 'Admin role assign';
        $permission->description = 'Permission to be able to assign roles to users';
        $permission->save();
        
        $this->attachToSuperMan($permission);
        $this->attachToAdmin($permission);
        
        $permission = new App\Model\Permission();
        $permission->name = 'flat-read';
        $permission->display_name = 'Flat read';
        $permission->description = 'Permission to be able to view own flat';
        $permission->save();
        
        $this->attachTFlatOwner($permission);
        
        $permission = new App\Model\Permission();
        $permission->name = 'flat-write';
        $permission->display_name = 'Flat update';
        $permission->description = 'Permission to be able to post status update for own flat';
        $permission->save();
        
        $this->attachTFlatOwner($permission);
    }
    
    private function attachToSuperMan($permission){
        if($this->superman_role){
            $this->superman_role->attachPermission($permission);
        }
    }
    
    private function attachToAdmin($permission){
        if($this->admin_role){
            $this->admin_role->attachPermission($permission);
        }
    }
    
    private function attachTFlatOwner($permission){
    	if($this->flat_owner){
    		$this->flat_owner->attachPermission($permission);
    	}
    }
}
