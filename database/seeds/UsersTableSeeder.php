<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Model\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superman = new User();
        $superman->email = 'superman@samify.com';
        $superman->name = 'Superman';
        $superman->password = bcrypt('superhero');
        $superman->confirmed = true;
        $superman->enabled = true;
        $superman->save();


        $superman_role = new Role();
        $superman_role->name = 'superman';
        $superman_role->display_name = 'Super man';
        $superman_role->description = 'Man who can fly around JCC';
        $superman_role->save();
        
        $admin_role = new Role();
        $admin_role->name = 'admin';
        $admin_role->display_name = "Administrator";
        $admin_role->description = "Administrator users";
        $admin_role->save();
        
        $flat_owner = new Role();
        $flat_owner->name = 'flatowner';
        $flat_owner->display_name = "Flat Owner";
        $flat_owner->description = "Owner of flat(s) in JCC";
        $flat_owner->save();

        $superman->attachRole($superman_role);
        $superman->attachRole($admin_role);
    }
}
