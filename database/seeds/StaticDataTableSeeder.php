<?php

use Illuminate\Database\Seeder;
use App\Model\Block;
use App\Model\Flat;
use App\Model\Corridor;
use App\Model\Parking;
use App\Model\Lift;

class StaticDataTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $blockA = Block::create(['name' => 'A']);
        $blockH = Block::create(['name' => 'H']);
        $blockI = Block::create(['name' => 'I']);
        $blockJ = Block::create(['name' => 'J']);
        $blockK = Block::create(['name' => 'K']);

        $this->command->info('Blocks inserted!');

        $this->createFlats($blockA);
        $this->createFlats($blockH);
        $this->createFlats($blockI);
        $this->createFlats($blockJ);
        $this->createFlats($blockK);

        $this->createCorridors($blockA);
        $this->createCorridors($blockH);
        $this->createCorridors($blockI);
        $this->createCorridors($blockJ);
        $this->createCorridors($blockK);

        $this->createParkings($blockA);
        $this->createParkings($blockH);
        $this->createParkings($blockI);
        $this->createParkings($blockJ);
        $this->createParkings($blockK);

        $this->createLifts($blockA);
        $this->createLifts($blockH);
        $this->createLifts($blockI);
        $this->createLifts($blockJ);
        $this->createLifts($blockK);
    }

    private function createFlats($block) {
        $flatCount = 0;
        if ($block->name == 'A') {
            //BLOCK A, 8 Flats Each floor, Total Floor 13 = Number of flats = 104
            $flatCount = 8;
        } else if ($block->name == 'H') {
            //BLOCK H=2, 11 Flats Each Floor, Total Floor 13 = Number of flats = 143
            $flatCount = 11;
        } else if ($block->name == 'I') {
            //BLOCK I=3, 12 Flats Each Floor, Total Floor 13 = Number of flats = 156
            $flatCount = 12;
        } else if ($block->name == 'J') {
            //BLOCK J=3, 12 Flats Each Floor, Total Floor 13 = Number of flats = 156
            $flatCount = 12;
        } else if ($block->name == 'K') {
            //BLOCK K=5, 10 Flats Each Floor, Total Floor 13 = Number of flats = 130
            $flatCount = 10;
        }

        $totalFlats = 0;

        for ($floor = 1; $floor <= 13; $floor++) {
            for ($i = 1; $i <= $flatCount; $i++) {
                Flat::create(['BLOCK_ID' => $block->id, 'FLOOR' => $floor, 'UNIT' => $i]);
                $totalFlats++;
            }
        }

        $this->command->info(' [' . $totalFlats . '] Flats inserted for Block ' . $block->name);
    }

    private function createCorridors($block) {

        $total = 0;

        for ($floor = 1; $floor <= 13; $floor++) {
            Corridor::create(['BLOCK_ID' => $block->id, 'FLOOR' => $floor]);

            $total++;
        }

        $this->command->info(' [' . $total . '] Corridors inserted for Block ' . $block->name);
    }

    private function createParkings($block) {
        $total = 0;

        for ($floor = 1; $floor <= 3; $floor++) {
            Parking::create(['BLOCK_ID' => $block->id, 'FLOOR' => 'B'.$floor]);

            $total++;
        }

        $this->command->info(' [' . $total . '] Parking(s) inserted for Block ' . $block->name);
    }

    private function createLifts($block) {
        $total = 0;

        for ($lift = 1; $lift <= 3; $lift++) {
            Lift::create(['BLOCK_ID' => $block->id, 'LIFTNUMBER' => $lift]);

            $total++;
        }

        $this->command->info(' [' . $total . '] Lift(s) inserted for Block ' . $block->name);
    }

}
