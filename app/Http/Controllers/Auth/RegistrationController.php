<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Crypt;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Mail;
use Log;

class RegistrationController extends BaseController {
    
	public function __construct(\Illuminate\Contracts\Auth\PasswordBroker $broker)
	{
		$this->broker = $broker;
	}
	
	/*
	 |--------------------------------------------------------------------------
	 | Registration Controller
	 |--------------------------------------------------------------------------
	 |
	 | This controller handles the registration of new users, as well as the
	 | authentication of existing users. By default, this controller uses
	 | a simple trait to add these behaviors. Why don't you explore it?
	 |
	 */
	use RegistersUsers;
	
	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/admin/users';
	
    /**
     * Check for confirmation code and redirect to Login.
     * @param type $confirmation_code
     * @return type
     */
    public function confirm($confirmation_code, $email)
    {
        if( ! $confirmation_code)
        {
            Flash::error('Unathourized attempt is made.');
            return View('auth.login');
        }

        $user = User::where('confirmation_code', $confirmation_code)
                ->where('email', $email)->first();

        if ( ! $user)
        {
            Flash::error('Invalid confirmation code.');
            return View('auth.login');
//            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();

        Flash::message('You have successfully verified your account.');
        
        return $this->resetAndAskChangePassword($user);

    }
    
    /*
     * Action to be called when user attempts to login but have not verified
     * email address. This is rare case scenario as we don't revel password
     * when creating their account.
     * 
     * Call to this method requires a valid userid and it sends verification email
     * to the user with link to verify his/her email address.
     * 
     * Redirects to Login page.
     * 
     */
    public function emailVerificationPending($userid){
        if($userid){
            $user = User::where('id', Crypt::decrypt($userid))->first();
            
            $confirmation_code = str_random(30);
            $email = $user->email;
            $name = $user->name;
            $user->confirmed = false;//Not confirmed
            $user->confirmation_code = $confirmation_code;
            $user->save();
            
            Mail::send ( 
    			'email.verify', 
    			[ 
				'confirmation_code' => $confirmation_code,
				'email' => $user->email 
				], 
    			function ($message) use ($user) {
					$message->to ( $user->email, $user->name )->subject ( config('app.my_app_name').' - Verify email' );
				} 
    	);
            
            Flash::message('We\'ve resent confirmation code to your registered email address. '.$email);
            return Redirect::route('login');
        }
    }
    
    
    public function resetAndAskChangePassword($user)
    {
//     	//retrieve user
//     	$user = $this->broker->getUser($input);
    
//     	//no user found with email
//     	if (is_null($user)) {
//     		throw new Exception('User not found.');
//     	}
    
    	//get token
    	$token = $this->broker->createToken($user);
    	return redirect()->route('pwdreset', ['token'=>$token, 'email' => $user->getEmailForPasswordReset()]);
    }
    
    
}