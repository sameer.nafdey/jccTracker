<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Log;
use App\Model\Flat;
use App\Model\FlatOwner;
use App\UserRole;
use DB;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Model\Role;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home/flat';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            //'password' => 'required|min:6|confirmed',
            'flatid' => 'sometimes|unique:flat_owners,flat_id',
        ]);
        
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
//        Log::debug('Registering new user->'.$data);
//        $flat = DB::table('flats')->where('blockid', '=', $data['blockID'])->where('floor', '=', $data['floor'])->where('unit','=', $data['flatNumber'])->first();
//         $flat = Flat::find(Crypt::decrypt($data['flatid']));
        
//         $confirmation_code = str_random(30);
        
//         $user = User::create([
//             'name' => $data['name'],
//             'email' => $data['email'],
//             'password' => bcrypt('welcome2jccoa'), //we're setting this to ensure we can push user to change it when they will claim account. This password should not allow user to login.
//             'confirmation_code' => $confirmation_code,
//         ]);
        
//         $flatOwner = new FlatOwner();
//         $flatOwner->flat_id = $flat->id;
//         $flatOwner->user_id = $user->id;
//         $flatOwner->save();
        
//         Mail::send('email.verify', [$confirmation_code, 'email'=>$data['email']], function($message) {
//             $message->to($data['email'], $data['name'])
//                 ->subject('Verify your email address');
//         });
        
//         $role = Role::where('name','=','flat-owner')->first();
//         $user->attachRole($role);
        
//        $userRole = new UserRole();
//        $userRole->user_id = $user->id;
//        if($user->email==='sameer.nafdey@gmail.com' || $flat->id===163){
//            UserRole::create(['user_id'=>$user->id, 'role'=>'super-user', 'created_by'=>$user->id]);
//            UserRole::create(['user_id'=>$user->id, 'role'=>'admin', 'created_by'=>$user->id]);
//        }
//        $userRole->role = 'customer';
//        
//        $userRole->created_by = $user->id;
//        $userRole->save();
        
        return $user;
    }
    
    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);
        
        //check for user confirmation
//        $credentials['confirmed'] = 1;

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            $user = Auth::user();
            Log::debug("Is email verified and confirmed? ".$user->confirmed);
            if($user->confirmed==false){
            	Auth::logout();
            	
                return redirect()->route('login')->with('emailUserId', Crypt::encrypt($user->id));
            }
            if($user->enabled==false){
            	Auth::logout();
            	return redirect()->route('login')->with('account-disabled', true);
            }
            Log::debug($user->password);
            
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }
    
}
