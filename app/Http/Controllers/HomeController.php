<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Log;
use Auth;
use App\Model\FlatOwner;
use App\Model\EntityItemDefinition;
use App\Model\EntityItem;
use App\Model\Block;
use App\Model\Corridor;
use App\Model\Parking;
use App\Model\Lift;
use App\Model\Flat;
use Laracasts\Flash\Flash;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $user = Auth::user();
        $allFlatItemsDef = null;
        if ($user) {
            $flatOwner = FlatOwner::where('user_id', $user->id)->get();
            if (count($flatOwner) > 0) {
                $allFlatItemsDef = EntityItemDefinition::where('entity_type', 'flat')->get();
            }
        }
        if($allFlatItemsDef==null){
            Flash::info('There is no flat on record is owned by you. If this is an error, please contact JCCOA');
        }

        return view('users.flat')->with('ownedflats', $flatOwner)->with('allFlatItemDefinitions', $allFlatItemsDef);
    }

    public function blocksHome($id =null) {
        if ($id) {
            $selectedBlock = Block::find($id);
        } else {
            $user = Auth::user();
            if ($user) {
                $flatOwner = FlatOwner::where('user_id', $user->id)->first();
                if($flatOwner){
                    $selectedBlock = $flatOwner->flat()->first()->block()->first();
                }
            }else{
                
                $selectedBlock = Block::where('name', 'A');
            }
        }
        $allBlockItemsDef = EntityItemDefinition::where('entity_type', 'block')->get();
        
        return view('users.blocks')
                ->with('selectedBlock', $selectedBlock)
                ->with('allBlockItemDefinitions', $allBlockItemsDef);
    }

    public function corridorHome($blockId=null, $floorNumber=null) {
        
        $user = Auth::user();
        if ($user) {
            $flatOwner = FlatOwner::where('user_id', $user->id)->first();
            
        }
        if($blockId){
            $selectedBlock = Block::find($blockId);
        } else {
            if($flatOwner){
                $selectedBlock = $flatOwner->flat()->first()->block()->first();
            }else{
                $selectedBlock = Block::where('name', 'A')->first();
            }
        }
        
        if($selectedBlock){
            if($floorNumber){
                $selectedCorridor = Corridor::where('block_id', $selectedBlock->id)->where('floor', $floorNumber)->first();
            }else if($flatOwner){
                $selectedCorridor = Corridor::where('block_id', $selectedBlock->id)->where('floor', $flatOwner->flat()->first()->floor)->first();
            }
            $allCorridorsInSelectedBlock = Corridor::where('block_id', $selectedBlock->id)->get();
        }
        
        $allCorridorItemsDef = EntityItemDefinition::where('entity_type', 'corridor')->get();
        
        return view('users.corridors')
                ->with('selectedBlock', $selectedBlock)
                ->with('selectedCorridor', $selectedCorridor)
                ->with('allCorridorsInSelectedBlock', $allCorridorsInSelectedBlock)
                ->with('allCorridorItemDefinitions', $allCorridorItemsDef);
    }

    public function parkingHome($blockId=null, $floorNumber=null) {
        
        $user = Auth::user();
        if ($user) {
            $flatOwner = FlatOwner::where('user_id', $user->id)->first();
        }
        if($blockId){
            $selectedBlock = Block::find($blockId);
        } else {
            if($flatOwner){
                $selectedBlock = $flatOwner->flat()->first()->block()->first();
            }else{
                $selectedBlock = Block::where('name', 'A')->first();
            }
        }
        
        if($selectedBlock){
            if($floorNumber){
                $selectedCorridor = Parking::where('block_id', $selectedBlock->id)->where('floor', $floorNumber)->first();
            }else{
                $selectedCorridor = Parking::where('block_id', $selectedBlock->id)->where('floor', 'B1')->first();
            }
            $allCorridorsInSelectedBlock = Parking::where('block_id', $selectedBlock->id)->get();
        }
        
        $allCorridorItemsDef = EntityItemDefinition::where('entity_type', 'parking')->get();
        
        return view('users.parking')
                ->with('selectedBlock', $selectedBlock)
                ->with('selectedCorridor', $selectedCorridor)
                ->with('allCorridorsInSelectedBlock', $allCorridorsInSelectedBlock)
                ->with('allCorridorItemDefinitions', $allCorridorItemsDef);
    }

    public function liftsHome($blockId=null, $liftNumber=null) {
        $user = Auth::user();
        if ($user) {
            $flatOwner = FlatOwner::where('user_id', $user->id)->first();
        }
        if($blockId){
            $selectedBlock = Block::find($blockId);
        } else {
            if($flatOwner){
                $selectedBlock = $flatOwner->flat()->first()->block()->first();
            }else{
                $selectedBlock = Block::where('name', 'A')->first();
            }
        }
        
        if($selectedBlock){
            if($liftNumber){
                $selectedLift = Lift::where('block_id', $selectedBlock->id)->where('liftnumber', $liftNumber)->first();
            }else{
                $selectedLift = Lift::where('block_id', $selectedBlock->id)->where('liftnumber', 1)->first();
            }
            $allLiftInSelectedBlock = Lift::where('block_id', $selectedBlock->id)->get();
        }
        
        $allLiftItemsDef = EntityItemDefinition::where('entity_type', 'lift')->get();
        
        return view('users.lifts')
                ->with('selectedBlock', $selectedBlock)
                ->with('selectedLift', $selectedLift)
                ->with('allLiftInSelectedBlock', $allLiftInSelectedBlock)
                ->with('allLiftItemDefinitions', $allLiftItemsDef);
        
    }

    public function otherFlatsHome() {
        return view('users.flat-others');
    }

}
