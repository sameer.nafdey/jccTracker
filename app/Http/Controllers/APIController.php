<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Model\Block;
use App\Model\Corridor;
use App\Model\Flat;
use App\Model\FlatOwner;
use App\Model\Parking;
use App\Model\Lift;
use App\User;
use Crypt;
use Mail;

class APIController extends Controller {

    public function getFlatID(Request $request) {
        $blk = $request->input('blockId');
        $floor = $request->input('floor');
        $fltNumber = $request->input('flatNumber');
        
        $flat = Flat::where('block_id', $blk)
                        ->where('floor', $floor)
                        ->where('unit', $fltNumber)->first();
        $flatOwner = FlatOwner::where('flat_id', $flat->id)->first();
        if ($flatOwner) {
            return response()->json(['owner-exist'=>true, 'owner-name'=>  User::where('id', $flatOwner->user_id)->first()->name]);
        } else {
            return response()->json(['owner-exist'=>false, 'flatid'=>Crypt::encrypt($flat->id)]);
        }
    }
    
    public function getFloorsDropdown($corridorOrParkingOrLift, $blockId){
        if($blockId){
            $selectedBlock = Block::find($blockId);
        } 
        
        if($selectedBlock){
            if($corridorOrParkingOrLift==='corridor'){
                $allCorridorsInSelectedBlock = Corridor::where('block_id', $selectedBlock->id)->get();
            }else if($corridorOrParkingOrLift==='parking'){
                $allCorridorsInSelectedBlock = Parking::where('block_id', $selectedBlock->id)->get();
            }else if($corridorOrParkingOrLift==='lift'){
                $allCorridorsInSelectedBlock = Lift::where('block_id', $selectedBlock->id)->get();
            }
            
        }
        
        if($corridorOrParkingOrLift==='lift'){
            return view('users.sections.lift-dropdown')
                ->with('selectedBlock', $selectedBlock)
                ->with('allCorridorsInSelectedBlock', $allCorridorsInSelectedBlock);
        }else{
            return view('users.sections.corridor-dropdown')
                ->with('selectedBlock', $selectedBlock)
                ->with('allCorridorsInSelectedBlock', $allCorridorsInSelectedBlock);
        }
        
    }
    
    public function getVerifyEmailPreview(Request $request){
    	$userID = $request->input('userId'); 
    	$mailSubject = $request->input('mailSubject');
    	$additionalMessage = $request->input('additionalMessage');
    	if($userID){
    		$user = User::find ( $userID );
    		if ($user) {
    			$confirmation_code = str_random(30);
    			return view('email.verify')->with('confirmation_code', $confirmation_code)->with('email', $user->email)->with('mailSubject',  $mailSubject)->with('additionalMessage', $additionalMessage);
    		}
    	}
    }
    
    public function sendVerificationEmail(Request $request){
    	$userID = $request->input('userId'); 
    	$mailSubject = $request->input('mailSubject');
    	$additionalMessage = $request->input('additionalMessage');
    	if($userID){
    		$user = User::find ( $userID );
    		if ($user) {
    			$confirmation_code = str_random(30);
    			$user->confirmation_code = $confirmation_code;
    			$user->save();
    		
    			Mail::send (
    				'email.verify',
    				[
    						'confirmation_code' => $confirmation_code,
    						'email' => $user->email,
    						'mailSubject' => $mailSubject,
    						'additionalMessage' => $additionalMessage
    				],
    				function ($message) use ($user, $mailSubject){
    					$message->to ( $user->email, $user->name )->subject ( $mailSubject!='' ? $mailSubject : 'Welcome to '.config('app.my_app_name') );
    				}
    				);
    		}
    	}
    }

}
