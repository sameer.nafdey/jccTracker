<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\EntityItemDefinition;
use DB;
use Log;

class AdminController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function items() {
        return view('admin.items');
    }

    public function getEntityItemsDefByType(Request $request) {
//        Log::debug($request->input());
//        DB::connection()->enableQueryLog();
//        $defList = DB::table('entity_item_definitions')->where('entity_type', '=', $request->input('entity_type'))->get();
//        Log::debug(DB::getQueryLog());
        $defList = EntityItemDefinition::where('entity_type', $request->input('entity_type'))->get();
        return view('admin.itemDefList')->with('definitionsByType', $defList);
    }

    public function store(Request $request) {

        $definition = new EntityItemDefinition();
        $definition->entity_type = $request->input('entity_type');
        $definition->title = $request->input('title');
        $definition->description = $request->input('desc');
        $definition->save();
        return $this->getEntityItemsDefByType($request);
    }

    public function remove(Request $request) {
        
        $id = $request->input('id');
        EntityItemDefinition::destroy($id);
        return $this->getEntityItemsDefByType($request);
    }

    public function update(Request $request) {
        
        $id = $request->input('id');
        $title = $request->input('title');
        $desc = $request->input('desc');
        $object = EntityItemDefinition::find($id);
        if ($object) {
            $object->title = $title;
            $object->description = $desc;
            $object->save();
        }
        return $this->getEntityItemsDefByType($request);
    }
    
    public function getUser($id){
        
        $users = null;
        if($id=='all'){
            $users = \App\User::all();
        }
        return view('admin.userList')->with('usersList', $users);
    }

}
