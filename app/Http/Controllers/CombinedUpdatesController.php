<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\EntityItem;
use App\Model\EntityItemDefinition;
use App\Model\EntityItemStatus;
use Log;
use Auth;
use App\Model\Flat;
use App\Model\Corridor;
use App\Model\Block;
use App\Model\Parking;
use App\Model\Lift;
use Carbon\Carbon;

class CombinedUpdatesController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function combinedUpdateFlats() {

        $allEntityItemsDef = EntityItemDefinition::where('entity_type', 'flat')->get();

        return view('users.combined-updates.flatsHome')
                        ->with('allItemDefinitions', $allEntityItemsDef);
    }

    public function combinedUpdateBlocks() {
        $allEntityItemsDef = EntityItemDefinition::where('entity_type', 'block')->get();
        return view('users.combined-updates.blocksHome')->with('allItemDefinitions', $allEntityItemsDef);
    }

    public function combinedUpdateCorridor() {
        $allEntityItemsDef = EntityItemDefinition::where('entity_type', 'corridor')->get();
        return view('users.combined-updates.corridorsHome')
                ->with('allItemDefinitions', $allEntityItemsDef);
    }

    public function combinedUpdateParking() {
        
        $allEntityItemsDef = EntityItemDefinition::where('entity_type', 'parking')->get();
        return view('users.combined-updates.parkingHome')
                ->with('allItemDefinitions', $allEntityItemsDef);
    }

    public function combinedUpdateLifts($blockId = null, $floorNumber = null) {
        $allEntityItemsDef = EntityItemDefinition::where('entity_type', 'lift')->get();
        return view('users.combined-updates.liftsHome')
                ->with('allItemDefinitions', $allEntityItemsDef);
    }

    public function updateFlats(Request $request) {
        $flats = $request->input('flatsList');
        $blockId = $request->input('blockId');
        
        $itemDefId = $request->input('itemDefId');

        if ($flats && $blockId && $itemDefId) {
            foreach ($flats as $unit) {
                
                $status = $unit['status'];
                
                $flat = Flat::where('id', $unit['flatid'])->first();
                if ($flat) {
                    
                    //valid flat
                    $entityItem = $this->handleEntityItem($flat->id, $itemDefId, 'flat');

                    $entityStatus = new EntityItemStatus();
                    $entityStatus->entity_item_id = $entityItem->id;
                    $entityStatus->status = $status;
                    $entityStatus->recorded_by = Auth::user()->id;
                    $entityStatus->recorded_at = Carbon::now();
                    $entityStatus->save();
                }else{
                    return response()->json(['error'=>'Invalid flat details']);
                }
            }
        }else{
            return response()->json(['error'=>'Invalid flat details']);
        }
        
        return response()->json(['success'=>'Records are updated. Thank you!']);
        
    }
    
    public function updateCorridors(Request $request){
        $corridorsList = $request->input('corridorsList');
        $blockId = $request->input('blockId');
        $itemDefId = $request->input('itemDefId');
        
        
        if($blockId && $corridorsList && $itemDefId){
            foreach ($corridorsList as $idx=>$corridorStatus){
                
                $corridor = Corridor::where('block_id', $blockId)
                        ->where('floor', $idx)
                        ->first();
                if($corridor){
                    //valid corridor
                    $entityItem = $this->handleEntityItem($corridor->id, $itemDefId, 'corridor');

                    $entityStatus = new EntityItemStatus();
                    $entityStatus->entity_item_id = $entityItem->id;
                    $entityStatus->status = $corridorStatus;
                    $entityStatus->recorded_by = Auth::user()->id;
                    $entityStatus->recorded_at = Carbon::now();
                    $entityStatus->save();
                }else{
                    return response()->json(['error'=>'Invalid corridor detail - Block='+$blockId+',Floor='+$idx]);
                }
            }
        }else{
            return response()->json(['error'=>'Invalid status details']);
        }
        return response()->json(['success'=>'All status recorded successfully. Thank you!']);
    }
    
    public function updateBlocks(Request $request) {
        $blocks = $request->input('blockList');
        $itemDefId = $request->input('itemDefId');
        
        if ($blocks && $itemDefId) {
            foreach ($blocks as $block) {
                
                $status = $block['status'];
                
                $blk = Block::where('id', $block['blockid'])->first();
                if ($blk) {
                    
                    //valid flat
                    $entityItem = $this->handleEntityItem($blk->id, $itemDefId, 'block');

                    $entityStatus = new EntityItemStatus();
                    $entityStatus->entity_item_id = $entityItem->id;
                    $entityStatus->status = $status;
                    $entityStatus->recorded_by = Auth::user()->id;
                    $entityStatus->recorded_at = Carbon::now();
                    $entityStatus->save();
                }else{
                    return response()->json(['error'=>'Invalid block details']);
                }
            }
        }else{
            return response()->json(['error'=>'Invalid block details']);
        }
        
        return response()->json(['success'=>'Records are updated. Thank you!']);
        
    }
    
    public function updateParkings(Request $request) {
        $parkings = $request->input('parkingList');
        $itemDefId = $request->input('itemDefId');
        
        if ($parkings && $itemDefId) {
            foreach ($parkings as $parking) {
                
                $status = $parking['status'];
                
                $blk = Parking::where('id', $parking['parkingid'])->first();
                if ($blk) {
                    
                    //valid flat
                    $entityItem = $this->handleEntityItem($blk->id, $itemDefId, 'parking');

                    $entityStatus = new EntityItemStatus();
                    $entityStatus->entity_item_id = $entityItem->id;
                    $entityStatus->status = $status;
                    $entityStatus->recorded_by = Auth::user()->id;
                    $entityStatus->recorded_at = Carbon::now();
                    $entityStatus->save();
                }else{
                    return response()->json(['error'=>'Invalid parking details']);
                }
            }
        }else{
            return response()->json(['error'=>'Invalid parking details']);
        }
        
        return response()->json(['success'=>'Records are updated. Thank you!']);
        
    }
    
    public function updateLifts(Request $request) {
        $lifts = $request->input('liftList');
        $itemDefId = $request->input('itemDefId');
        
        if ($lifts && $itemDefId) {
            foreach ($lifts as $lift) {
                
                $status = $lift['status'];
                
                $blk = Lift::where('id', $lift['liftid'])->first();
                if ($blk) {
                    
                    //valid flat
                    $entityItem = $this->handleEntityItem($blk->id, $itemDefId, 'lift');

                    $entityStatus = new EntityItemStatus();
                    $entityStatus->entity_item_id = $entityItem->id;
                    $entityStatus->status = $status;
                    $entityStatus->recorded_by = Auth::user()->id;
                    $entityStatus->recorded_at = Carbon::now();
                    $entityStatus->save();
                }else{
                    return response()->json(['error'=>'Invalid lift details']);
                }
            }
        }else{
            return response()->json(['error'=>'Invalid lift details']);
        }
        
        return response()->json(['success'=>'Records are updated. Thank you!']);
        
    }
    
    private function handleEntityItem($flatId, $defId, $type) {
        $entityItem = EntityItem::where('entity_id', $flatId)
                        ->where('entity_type', $type)
                        ->where('item_def_id', $defId)->first();

        if ($entityItem) {
            Log::debug('entity already exist with id=' . $entityItem->id);
            Log::debug('entity_id=' . $entityItem->entity_id . ', entity_type=' . $entityItem->entity_type . ', item_def_id=' . $entityItem->item_def_id);
        } else {
            $entityItem = new EntityItem();

            $entityItem->entity_id = $flatId;
            $entityItem->entity_type = $type;
            $entityItem->item_def_id = $defId;
            $entityItem->save();
        }
        return $entityItem;
    }
    
    public function getFlatsWithStatus(Request $request){
        $selectedBlockId = $request->input('blockId');
        $selectedFloor = $request->input('floor');
        $selectedItemDefId = $request->input('itemDefId');
        $selectedItemType = $request->input('item_type');
        
        $allEntities = Flat::where('block_id', $selectedBlockId)
                ->where('floor', $selectedFloor)->get();
        $itemDef = EntityItemDefinition::where('id', $selectedItemDefId)->first();
        
        return view('users.combined-updates.combined-tracking')
                ->with('allEntities', $allEntities)
                ->with('itemDef', $itemDef)
                ->with('item_type', $selectedItemType);
    }
    
    public function getCorridorsWithStatus(Request $request){
        $selectedBlockId = $request->input('blockId');
        $selectedItemDefId = $request->input('itemDefId');
        $selectedItemType = $request->input('item_type');
        
        $allEntities = Corridor::where('block_id', $selectedBlockId)->get();
        $itemDef = EntityItemDefinition::where('id', $selectedItemDefId)->first();

        return view('users.combined-updates.combined-tracking')
                ->with('allEntities', $allEntities)
                ->with('itemDef', $itemDef)
                ->with('item_type', $selectedItemType);
    }
    
    public function getBlocksWithStatus(Request $request){
        $selectedItemDefId = $request->input('itemDefId');
        $selectedItemType = $request->input('item_type');
        $allEntities = Block::all();
        $itemDef = EntityItemDefinition::where('id', $selectedItemDefId)->first();
        
        return view('users.combined-updates.combined-tracking')
                ->with('allEntities', $allEntities)
                ->with('itemDef', $itemDef)
                ->with('item_type', $selectedItemType);
    }
    
    public function getParkingsWithStatus(Request $request){
        $selectedBlockId = $request->input('blockId');
        $selectedItemDefId = $request->input('itemDefId');
        $selectedItemType = $request->input('item_type');
        
        $allEntities = Parking::where('block_id', $selectedBlockId)->get();
        $itemDef = EntityItemDefinition::where('id', $selectedItemDefId)->first();

        return view('users.combined-updates.combined-tracking')
                ->with('allEntities', $allEntities)
                ->with('itemDef', $itemDef)
                ->with('item_type', $selectedItemType);
    }
    
    public function getLiftsWithStatus(Request $request){
        $selectedBlockId = $request->input('blockId');
        $selectedItemDefId = $request->input('itemDefId');
        $selectedItemType = $request->input('item_type');
        
        $allEntities = Lift::where('block_id', $selectedBlockId)->get();
        $itemDef = EntityItemDefinition::where('id', $selectedItemDefId)->first();

        return view('users.combined-updates.combined-tracking')
                ->with('allEntities', $allEntities)
                ->with('itemDef', $itemDef)
                ->with('item_type', $selectedItemType);
    }
}
