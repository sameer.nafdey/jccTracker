<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Permission;
use App\Model\Role;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Log;

class ManageRolesController extends Controller
{
	
	public function __construct() {
		$this->middleware('auth');
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        Log::debug(count($roles));
        return view('admin.rolesList')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.role")->with("allPermissions", Permission::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $theRole = new Role();
        $theRole->name = preg_replace('/\s+/', '', $request['roleName']);
        $theRole->description = $request['roleDescription'];
        $theRole->display_name = $request['roleName'];
        $theRole->save();
        
        $selectedPermission = Input::get('permission');
        
        foreach($selectedPermission as $p){
        	$perm = Permission::where('id', $p)->first();
        	Log::debug($perm);
        	if(is_object($perm)){
        		Log::debug('Key='.$perm->getKey());
        	}
        	
        	$theRole->attachPermission($perm);
        }
        
        Flash::info('Role is created.');
        return Redirect::to('admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$role = Role::find($id);
    	return view("admin.role")->with('selectedRole', $role)->with("allPermissions", Permission::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	Log::debug($request);
    	Log::debug($id);
    	
    	
    	
    	if($id){
    		$theRole = Role::where('id', $id)->first();
    		if($theRole){
    			$theRole->name = preg_replace('/\s+/', '', $request['roleName']);
    			$theRole->description = $request['roleDescription'];
    			$theRole->display_name = $request['roleName'];
    			Log::debug("Updated Role will have : ".$theRole);
    			
    			$theRole->save();
    			
    			$selectedPermission = Input::get('permission');
    			 
    			$allPermissions = Permission::all();
    			foreach($allPermissions as $p1){
    				if(in_array($p1->id, $selectedPermission)){
    					Log::debug("This permission selected: ".$p1);
    					if(!$theRole->hasPermission($p1->name)){
    						$theRole->attachPermission($p1);
    					}
    					
    				}else{
    					Log::debug("This permission not selected: ".$p1);
    					Log::debug("Does user have this? ");
    					if($theRole->hasPermission($p1->name)){
    						$theRole->detachPermission($p1);
    					}
    				}
    			
    			}
    		}
    		
    	}
    	Flash::info('Role is updated.');
    	return Redirect::to('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::debug("Request to delete: ".$id);
        if($id){
        	$theRole = Role::where('id', $id)->first();
        	if($theRole){
        		$theRole->delete();
        	}
        }
        Flash::info('Role is deleted.');
        return Redirect::to('admin/roles');
    }
}
