<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Model\EntityItem;
use App\Model\EntityItemStatus;
use Auth;
use Carbon\Carbon;

class TrackingController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function updateFlatStatus(Request $request) {
        
        $data = $request->input('data');
        $this->updateStatus($data, 'flat');
        return '/home/flat';
    }

    public function updateBlockStatus(Request $request) {
        
        $data = $request->input('data');
        $block_id = $this->updateStatus($data, 'block');

        return '/home/blocks/' . $block_id;
    }

    public function updateCorridorOrParkingStatus(Request $request) {

        $block_id = $request->input('param1');
        $floor = $request->input('param2');
        $corridorOrParking = $request->input('param3');

        $data = $request->input('data');
        $this->updateStatus($data, $corridorOrParking);

        return '/home/' . $corridorOrParking . '/' . $block_id . '/' . $floor;
    }

    private function updateStatus($data, $entity_type) {
        $return_entityId = '';
        foreach ($data as $idx => $value) {
            if (!$value) {
                continue;
            }

            //this is although used only for block for now.
            $return_entityId = $value['entity_id'];

            $entityItem = EntityItem::where('entity_id', $value['entity_id'])
                    ->where('entity_type', $entity_type)
                    ->where('item_def_id', $value['defId'])->first();
            if ($entityItem) {
                Log::debug('entity already exist with id='.$entityItem->id);
                Log::debug('entity_id='.$entityItem->entity_id.', entity_type='.$entityItem->entity_type.', item_def_id='.$entityItem->item_def_id);
            }else{
                $entityItem = new EntityItem();
                
                $entityItem->entity_id = $value['entity_id'];
                $entityItem->entity_type = $entity_type;
                $entityItem->item_def_id = $value['defId'];
                $entityItem->save();
            }
            


            $entityStatus = new EntityItemStatus();
            $entityStatus->entity_item_id = $entityItem->id;
            $entityStatus->status = $value['status'];
            $entityStatus->recorded_by = Auth::user()->id;
            $entityStatus->recorded_at = Carbon::now();
            $entityStatus->save();
        }
        return $return_entityId;
    }

}
