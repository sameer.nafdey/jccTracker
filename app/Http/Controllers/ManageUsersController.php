<?php

namespace App\Http\Controllers;

use App\Model\Flat;
use App\Model\FlatOwner;
use App\Model\Role;
use App\User;
use App\UserSearch;
use Crypt;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Log;
use App\Model\AllFlats;
use Illuminate\Database\Eloquent\Collection;
use function GuzzleHttp\json_encode;
use Redirect;

class ManageUsersController extends Controller
{
	
	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/admin/users';
	
	public function __construct() {
		$this->middleware('auth');
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$allUsers = UserSearch::all();
        return view('admin.usersearch')->with('allUsers', $allUsers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->register($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::debug("show called in ManageUserRegistController  ".$id);
        
        $allFlats = AllFlats::all();
        $allFlatArr = array();
         
        foreach ($allFlats as $f){
        	
        	$setFlatID = Crypt::encrypt($f->FlatID);
        	
        	$fArr = array(
        			'id'			=> $setFlatID,
        			'text'			=> ($f->Block).'-'.($f->Floor).($f->Unit<10?'0'.$f->Unit:$f->Unit).($f->FlatOwnerID!=null ? ' (Owned by someone)' :''),
        			'flatOwnerId'	=> ($f->FlatOwnerID!=null ? Crypt::encrypt($f->FlatOwnerID) : ''),
        			'disabled'		=> ($f->FlatOwnerID!=null? true : false)
        	);
        
        	array_push($allFlatArr, $fArr);
        }
        
        if(is_numeric($id)){
        	
        }else if(is_string($id)){
        	if($id=="register"){
        		return $this->showRegistrationForm()->with('allFlats', json_encode($allFlatArr));
        	}
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id){
        	$userId = Crypt::decrypt($id);
        	$user = User::where('id', $userId)->first();
        	$allFlats = AllFlats::all();
        	$allFlatArr = array();
        	$userFlatID = "";
        	foreach ($allFlats as $f){
        		if($f->FlatOwnerID!=null && $f->FlatOwnerID==$user->id){
        			$userFlatID = Crypt::encrypt($f->FlatID);
        			$setFlatID = $userFlatID;
        		}else{
        			$setFlatID = Crypt::encrypt($f->FlatID);
        		}
        		
        		
        		
        		$fArr = array(
        				'id'=>($setFlatID),
        				'text'=> ($f->Block).'-'.($f->Floor).($f->Unit<10?'0'.$f->Unit:$f->Unit).($f->FlatOwnerID!=null? $f->FlatOwnerID==$user->id? ' ('.$user->name."'s flat)" : ' (Owned by someone)' :''),
        				'flatOwnerId'=>($f->FlatOwnerID!=null?Crypt::encrypt($f->FlatOwnerID):''),
        				'disabled'=> ($f->FlatOwnerID!=null? $f->FlatOwnerID==$user->id ? false: true:false)
        		);
        		
        		array_push($allFlatArr, $fArr);
        	}
        	
        	$allRoles = Role::all();
        	
        	return $this->showRegistrationForm()->with('selectedUser', $user)->with('allFlats', json_encode($allFlatArr))->with('ownerFlatID', $userFlatID)->with('allRoles', $allRoles);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::debug($request);
        Log::debug($request->input('id'));
        
        $messages = [
        		'flatDetailsNeeded.required_with' => 'Flat details are required if user is flat owner!',
        ];
         
        $registrationRules = [
        		'id' => 'required|string',
        		'name' => 'required|max:255',
        		'flatId' => 'required_if:isFlatOwner,true',
        		'isActivated' => 'required'
        ];
         
        $validator = Validator::make($request->all(), $registrationRules, $messages);
        
        
    	if ($validator->fails()) {
    		Log::debug("Validation failed?");
    		Log::debug("Errors:".$validator->errors());
    		return Redirect::back()->withErrors($validator->errors());
    	}else{
    		$userId = Crypt::decrypt ( $request->input ( 'id' ) );
    		$flatId = $request->input ( 'flatId' ) ? Crypt::decrypt ( $request->input ( 'flatId' ) ) : null;
    		
    		if ($userId) {
    			$user = User::find ( $userId );
    			if ($user) {
    		
    				//1. Updating flat details
    		
    				$usersFlats = $user->getFlatsOwned ()->keyBy ( 'flat_id' );
    				if ($flatId && $usersFlats->get ( $flatId )) {
    					// already owns this flat so no change.
    				} else if ($flatId && ! $usersFlats->get ( $flatId )) {
    					// this flat is not owned by the user.
    					// note we already validate in validator that the said flat is not already owned by someone else in flat_owners
    					// so we can safely designate him as new owner here.
    						
    					// for now we don't want to allow ownership of multiple flats.
    					// so just update existing FlatOwner
    					if (! $usersFlats->isEmpty ()) {
    						$flat = $user->getFlatsOwned()->first();
    						$flat->flat_id = $flatId;
    						$flat->save();
    					}else{
    						$flat = Flat::find ( $flatId );
    						$flatOwner = new FlatOwner ();
    						$flatOwner->flat_id = $flat->id;
    						$flatOwner->user_id = $user->id;
    						$flatOwner->save();
    		
    						//if he did not owned a flat earlier
    						if(!$user->hasRole('flatowner')){
    							$role = Role::where('name','=','flatowner')->first();
    							$user->attachRole($role);
    						}
    					}
    						
    				} else if(!$usersFlats->isEmpty() && $user->hasRole('flatowner')) {
    					$role = Role::where('name','=','flatowner')->first();
    					$user->detachRole($role);
    					$flat = $user->getFlatsOwned()->first();
    					$flat->delete();
    				}
    				//2. Update name not email.
    				$user->name = $request->input('name');
    				$user->enabled = $request->input('isActivated')=="false"?false:true;
    				$user->save();
    				
    				if($request->input('roles') && !empty($request->input('roles'))){
    					$user->detachRoles();
    					foreach ($request->input('roles') as $roleIDToSet) {
    						$role = Role::find(Crypt::decrypt ($roleIDToSet));
    							
    						$user->attachRole($role);
    					}
    				}
    			}
    		}
    		return $this->index();
    	}
		
    	
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
    	$messages = [
    			'flatId.required_with' => 'Flat details are required if user is flat owner!',
    	];
    	
    	$registrationRules = [
					    			'name' => 'required|max:255',
					    			'email' => 'required|email|max:255|unique:users',
					    			'flatId' => 'sometimes|unique:flat_owners,flat_id',
					    			'flatId' => 'required_if:isFlatOwner,true',
    								'isActivated' => 'required'
					    	 ];
    	
    	$validator = Validator::make($data, $registrationRules, $messages);
    	
    	
    
    	return $validator;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
    	Log::debug('Registering new user->'.json_encode($data));
    	
    	//        $flat = DB::table('flats')->where('blockid', '=', $data['blockID'])->where('floor', '=', $data['floor'])->where('unit','=', $data['flatNumber'])->first();
    	
    
    	$confirmation_code = str_random(30);
    
    	$user = User::create([
    			'name' => $data['name'],
    			'email' => $data['email'],
    			'password' => bcrypt('welcome2jccoa'), //we're setting this to ensure we can push user to change it when they will claim account. This password should not allow user to login.
    			'confirmation_code' => $confirmation_code,
    			'confirmed' => false,
    			'enabled' => $data['isActivated']=="false"?false:true
    	]);
    	
    
    	if(isset($data['flatId'])){
    		$flat = Flat::find(Crypt::decrypt($data['flatId']));
    		$flatOwner = new FlatOwner();
    		$flatOwner->flat_id = $flat->id;
    		$flatOwner->user_id = $user->id;
    		$flatOwner->save();
    		
    		$role = Role::where('name','=','flatowner')->first();
    		$user->attachRole($role);
    	}
    	 
    	
    /* This code has been tested but we want to give invite button functionality where this email along with additional content will be sent to user.
     * commenting this for now.
    	Mail::send ( 
    			'email.verify', 
    			[ 
				'confirmation_code' => $confirmation_code,
				'email' => $data ['email'] 
				], 
    			function ($message) use ($user) {
					$message->to ( $user->email, $user->name )->subject ( 'Welcome to '.config('app.my_app_name') );
				} 
    	);
     */
    
    		return $user;
    }
    
    /**
     * Overriding this method from RegistersUser trair to 
     * Handle a registration request for the application.
     * 
     * Essentially this overrides automatic login of the regisgtered user
     * its important since we don't have open registration.
     * 
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
    	Log::debug("ManageUsersController's register() called");
    	$validator = $this->validator($request->all());
    
    	if ($validator->fails()) {
    		$this->throwValidationException(
    				$request, $validator
    				);
    	}
    
    	$this->create($request->all());
    
    	return redirect($this->redirectPath());
    }
    
}
