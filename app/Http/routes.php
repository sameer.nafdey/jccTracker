<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        return redirect()->route('everyoneshome');
    }else{
        return view('welcome');
    }
    
});

// Authentication Routes...
Route::get('login', ['as'=>'login', 'uses'=>'Auth\AuthController@showLoginForm']);
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
//Route::get('register', 'Auth\AuthController@showRegistrationForm');
//Route::post('register', 'Auth\AuthController@register');

// Password Reset Routes...
Route::get('password/reset/{token?}', ['as'=>'pwdreset', 'uses'=>'Auth\PasswordController@showResetForm']);
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail'); //creates reset token as well as sends email
Route::post('password/reset', 'Auth\PasswordController@reset'); //just shows the reset form 

Route::get('verify/{confirmationCode}/{email}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegistrationController@confirm'
]);

Route::get('/resend/{userid}', ['as'=>'email-verify-pending', 'uses'=>'Auth\RegistrationController@emailVerificationPending']);

Route::get('/flatid', 'APIController@getFlatID');
Route::get('/api/uiview/get/{corridorOrParking}/{blockId}', 'APIController@getFloorsDropdown');
Route::get('/api/verifyemail/preview', 'APIController@getVerifyEmailPreview');
Route::post('/api/verifyemail/send', 'APIController@sendVerificationEmail');

//Route::auth();

Route::get('/home/flat', ['as'=>'everyoneshome', 'uses'=> 'HomeController@index']);


Route::group(['prefix'=>'admin', 'middleware' => ['role:admin']],
    function(){
        Route::get('items', 'AdminController@items');
        Route::get('item/definition/get', 'AdminController@getEntityItemsDefByType');
        Route::post('item/definition/store', 'AdminController@store');
        Route::post('item/definition/remove', 'AdminController@remove');
        Route::post('item/definition/update', 'AdminController@update');
        Route::get('user/get/{id}', 'AdminController@getUser');
        
//         Route::get('users', 'ManageUsersController@index');
        
        Route::resource('roles', 'ManageRolesController');
        Route::resource('users', 'ManageUsersController');
    }
);



Route::post('/track/flat/update', 'TrackingController@updateFlatStatus');
Route::post('/track/block/update', 'TrackingController@updateBlockStatus');
Route::post('/track/corridor/update', 'TrackingController@updateCorridorOrParkingStatus');
Route::post('/track/parking/update', 'TrackingController@updateCorridorOrParkingStatus');
Route::post('/track/lift/update', 'TrackingController@updateCorridorOrParkingStatus');

Route::post('/track/combined/flats', 'CombinedUpdatesController@updateFlats');
Route::post('/track/combined/corridors', 'CombinedUpdatesController@updateCorridors');
Route::post('/track/combined/blocks', 'CombinedUpdatesController@updateBlocks');
Route::post('/track/combined/parkings', 'CombinedUpdatesController@updateParkings');
Route::post('/track/combined/lifts', 'CombinedUpdatesController@updateLifts');

Route::get('/home/blocks/{id?}', 'HomeController@blocksHome');
Route::get('/home/corridor/{blockId?}/{floorNumber?}', 'HomeController@corridorHome');
Route::get('/home/parking/{blockId?}/{floorNumber?}', 'HomeController@parkingHome');
Route::get('/home/lift/{blockId?}/{liftNumber?}', 'HomeController@liftsHome');
Route::get('/home/flat/other', 'HomeController@otherFlatsHome');

Route::get('/combined-updates/flats', 'CombinedUpdatesController@combinedUpdateFlats');
Route::get('/combined-updates/flats/getstatus', 'CombinedUpdatesController@getFlatsWithStatus');
Route::get('/combined-updates/blocks/', 'CombinedUpdatesController@combinedUpdateBlocks');
Route::get('/combined-updates/blocks/getstatus', 'CombinedUpdatesController@getBlocksWithStatus');
Route::get('/combined-updates/corridors/', 'CombinedUpdatesController@combinedUpdateCorridor');
Route::get('/combined-updates/corridors/getstatus', 'CombinedUpdatesController@getCorridorsWithStatus');

Route::get('/combined-updates/parkings', 'CombinedUpdatesController@combinedUpdateParking');
Route::get('/combined-updates/parkings/getstatus', 'CombinedUpdatesController@getParkingsWithStatus');
Route::get('/combined-updates/lifts', 'CombinedUpdatesController@combinedUpdateLifts');
Route::get('/combined-updates/lifts/getstatus', 'CombinedUpdatesController@getLiftsWithStatus');

//Route::resource('admin.users', 'ManageUsersController');

//Route::resource('admin.roles', 'ManageRolesController');