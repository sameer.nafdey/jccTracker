<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\Block;
use Log;
use DB;
use Validator;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

        //Using composer to fetch basic data that constructs json arrays for Blocks, Flats, Floors etc.
        $blocks = Block::all();

//        DB::connection()->enableQueryLog();
//        SELECT DISTINCT BLOCKID, UNIT FROM FLATS 
        $blockFlatUnits = DB::table('flats')->select('block_id', 'unit')->distinct('block_id')->get();
//            Log::debug(DB::getQueryLog());
//        Log::debug(json_encode($blockFlatUnits));
        view()->share('allBlocks', $blocks);
        view()->share('blockFlats', $blockFlatUnits);
//        Log::debug('All BLOCK' . json_encode($blocks));
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
