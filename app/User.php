<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable {

    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function isAdmin() {
        
        if ($this->hasRole('superman') || $this->hasRole('admin')) {
            return true;
        } else {
            return false;
        }
    }

    function isSuperUser() {
        
        if ($this->hasRole('superman')) {
            return true;
        } else {
            return false;
        }
    }
    
//    function roles(){
//        return $this->hasMany('App\UserRole');
//    }
    
    function getFlatsOwned(){
        return $this->hasMany('App\Model\FlatOwner')->get();
    }
    
    function isFlatOwner(){
    	return $this->getFlatsOwned()->count()>0?true:false;
    }

}
