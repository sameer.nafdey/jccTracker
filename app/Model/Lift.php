<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lift extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['block_id', 'liftnumber'];
    
    /**
     * Get parent Block of a Lift
     * @return type
     */
    public function block(){
        return $this->belongsTo('App\Model\Block');
    }
}
