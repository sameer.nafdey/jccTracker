<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Flat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['block_id', 'floor', 'unit'];
    
    /**
     * Get parent Block of a Flat
     * @return type
     */
    public function block(){
        return $this->belongsTo('App\Model\Block');
    }
    
    public function owners(){
        return $this->hasMany('App\Model\FlatOwner');
    }
    
    public function blockName(){
        return $this->block()->first()->name;
    }
    
    public function getReadableName(){
        $blockName = $this->blockName();
        $unit = $this->unit >=10? $this->unit : '0'.$this->unit;
        return $blockName.'-'.$this->floor.$unit;
    }
}
