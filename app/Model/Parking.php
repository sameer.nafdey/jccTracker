<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['block_id', 'floor'];
    
    /**
     * Get parent Block of a Parking
     * @return type
     */
    public function block(){
        return $this->belongsTo('App\Model\Block');
    }
}
