<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
    
    /**
     * Get flats belonging to a Block
     * @return type
     */
    public function flats(){
        return $this->hasMany('App\Model\Flat');
    }
    
    /**
     * Get parkings in a Block
     * @return type
     */
    public function parkings(){
        return $this->hasMany('App\Model\Parking');
    }
    
    /**
     * Get corridors belonging to a Block
     * @return type
     */
    public function corridors(){
        return $this->hasMany('App\Model\Corridor');
    }
    
    /**
     * Get lifts in a Block
     * @return type
     */
    public function lifts(){
        return $this->hasMany('App\Model\Lift');
    }
}
