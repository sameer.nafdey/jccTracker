<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AllFlats extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'all_flats';
}
