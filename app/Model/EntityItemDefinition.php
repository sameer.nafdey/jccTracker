<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EntityItemDefinition extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ENTITY_TYPE', 'TITLE', 'DESCRIPTION', 'CREATED_BY'];
    
    /**
     * Get all Entity Items of this definition.
     * @return type
     */
    public function entityItems(){
        return $this->hasMany('App\Model\EntityItem', 'item_def_id');
    }
}
