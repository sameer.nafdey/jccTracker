<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Corridor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['block_id', 'floor'];
    
    /**
     * Get parent Block of a Corridor
     * @return type
     */
    public function block(){
        return $this->belongsTo('App\Model\Block');
    }
}
