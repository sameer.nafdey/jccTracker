<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EntityItemStatus extends Model
{
    public $timestamps = false;
    /**
     * Get the Entity Item to which this status belongs to.
     * @return type
     */
    public function entity_item(){
        return $this->belongsTo('App\Model\EntityItem', 'entity_item_id');
    }
    
    /**
     * Get user who recorded the status.
     * @return type
     */
    public function recorded_by(){
        return $this->belongsTo('App\User', 'recorded_by');
    }
}
