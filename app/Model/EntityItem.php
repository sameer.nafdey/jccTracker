<?php

namespace App\Model;;

use Illuminate\Database\Eloquent\Model;

class EntityItem extends Model
{
    /**
     * Get Item Definition
     * @return type
     */
    public function entityItemDefinition(){
        return $this->belongsTo('App\Model\EntityItemDefinition', 'item_def_id');
    }
    
    /**
     * Get all statuses of current Entity Item.
     * @return type
     */
    public function entity_item_statuses(){
        return $this->hasMany('App\Model\EntityItemStatus', 'entity_item_id');
    }
    
    public function entity(){
        if($this->entity_type==='flat'){
            return $this->belongsTo('App\Model\Flat', 'entity_id');
        }else if($this->entity_type==='corridor'){
            return $this->belongsTo('App\Model\Corridor', 'entity_id');
        }else if($this->entity_type==='block'){
            return $this->belongsTo('App\Model\Block', 'entity_id');
        }else if($this->entity_type==='parking'){
            return $this->belongsTo('App\Model\Parking', 'entity_id');
        }else if($this->entity_type==='lift'){
            return $this->belongsTo('App\Model\Lift', 'entity_id');
        }
    }
}
