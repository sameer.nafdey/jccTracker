<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FlatOwner extends Model
{
    /**
     * Get owners of the flat; There can be Multiple Owners of any given flat.
     * @return type
     */
    public function owners(){
        return $this->hasMany('App\User', 'id', 'user_id');
    }
    
    /**
     * Get the flat.
     * @return type
     */
    public function flat(){
        return $this->belongsTo('App\Model\Flat');
    }
}
