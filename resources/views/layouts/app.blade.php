<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{config('app.my_app_name')}} @yield('title')</title>

        <!-- Fonts -->
        <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">-->
        <!--    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">-->

        <!-- Styles -->
        <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">-->
        
        <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:400,700|Courgette|Dancing+Script:700|Fjalla+One|Oxygen|Roboto" rel="stylesheet">
        <link rel="stylesheet" href='/css/font-awesome-4.5.0/font-awesome-4.5.0.min.css'/>
        <link rel="stylesheet" href="/css/bs/bootstrap.min.css"/>
        <link rel="stylesheet" href="/css/jasny-bootstrap.min.css" media="screen">
        <link rel="stylesheet" href="/css/tracker-common.css"/>
        
        <style type="text/css">
			body{
				font-family: 'Arima Madurai', cursive;
				background: url("{{URL::to('/')}}/images/brickwall.png");
			}
			
			#emailFooter{
				font-size: small;
			}
			
			.dropdown-backdrop {
			    position: static;
			}
			
			.collapsing-menu{
				border-bottom-color: #444;
    			border-bottom-style: solid;
    			border-bottom-width: 2px;
			}
			
			.submenu{
				color: black;
				text-shadow: 0 1px 0 rgba(255, 255, 255, 0.4);
				font-weight: bold;
			}
			
			.submenu>ul{
				padding-left: 0;
			}
			
			.submenu>ul>li.submenu-selected {
				font-weight: bolder;
				border-right-style: solid;
			    border-right-width: 5px;
			    border-right-color: blue;
			    background-color: #fff;
			}
			
			.submenu>ul>li{
				position: relative;
			    display: block;
			    padding: 10px 15px;
			    margin-bottom: -1px;
			    background-color: #eee;
			    border: 1px solid #ccc;
			}
			
			
			.mainmenu.selected {
				color: #fff;
			}
			
			.primary-menu-highlight{
				color: #000;
				background-color: #CCC;
			    border-bottom-color: #222;
			    border-bottom-style: solid;
			    border-top-color: #444;
			    border-top-style: solid;
			}
			
		</style>
        @yield('pageHeader')
        
    </head>

    <body id="app-layout">
    	<script src='/js/jquery-2.2.4.min.js'></script>
        <script src='/js/bootstrap.min.js'></script>

        @include('layouts.navigation')

        <div style="margin-left: 5%; margin-right: 5%;">
            @include('flash::message')
        </div>
        @yield('content')

        @include('layouts.footer')

        <!-- JavaScripts -->
    <!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>-->
        
        <script src="/js/jasny-bootstrap.min.js"></script>

        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    	<script src="/js/jcccommon.js"></script>

    @yield('pageJs')
</body>
</html>
