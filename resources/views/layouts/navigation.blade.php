<nav id="trackerNav" class="navmenu navmenu-default navmenu-fixed-left offcanvas-sm navmenu-inverse" role="navigation">
    <a class="navmenu-brand visible-md visible-lg" href="#" style="font-family: 'Courgette', cursive;color:white;">{{config('app.my_app_name')}}</a>
    <div id="mainMenu">
    <ul class="nav navmenu-nav">

        @if(Auth::check())
            @role('flatowner')
            <li class="collapsing-menu">
                <a href="#" 
                	data-toggle="collapse" 
                	data-parent="#mainMenu" 
                	data-target='#individualUpdates' 
                	aria-controls='individualUpdates' 
                	aria-expanded="false"
                	class="{{ Request::is('home/*') ? 'selected-nav' : '' }}"
                	>Dashboard <b class="caret"></b>
                </a>
                <div class="collapse submenu" id="individualUpdates">
	                <ul >
	                    <li class="{{ Request::is('home/flat') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{url('/home/flat')}}" style="color:#000; display:block;">Your flats</a>
	                    </li>
	                    <!--            <li><a href="{{url('/home/flat/other')}}">Other flats</a></li>-->
	                    <li class="{{ Request::is('home/blocks') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{url('/home/blocks')}}" style="color:#000; display:block;">Blocks</a>
	                    </li>
	                    <li class="{{ Request::is('home/corridor') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{url('/home/corridor')}}" style="color:#000; display:block;">Corridors</a>
	                    </li>
	                    <li class="{{ Request::is('home/parking') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{url('/home/parking')}}" style="color:#000; display:block;">Parking Floors</a>
	                    </li>
	                    <li class="{{ Request::is('home/lift') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{url('/home/lift')}}" style="color:#000; display:block;">Lifts</a>
	                    </li>
	                </ul>
                </div>
            </li>
            @endrole
            @role('admin')
            <li class="collapsing-menu">
            	<a href="#" 
            	data-toggle="collapse" 
            	data-parent="#mainMenu" 
            	data-target='#combinedUpdate' 
            	aria-controls='combinedUpdate' 
            	aria-expanded="false"
            	class="{{ Request::is('combined-updates/*') ? 'selected-nav' : '' }}"
            	>Combined Updates <b class="caret"></b>
            	</a>
    			<div class="collapse submenu" id="combinedUpdate">
    				<ul class="" >
	                    <li class="{{ Request::is('combined-updates/flats') ? 'submenu-selected' : '' }}">
	                    	<a href="{{ url('combined-updates/flats') }}" style="color:#000; display:block;">Flats</a>
	                    </li>
	                    <li class="{{ Request::is('combined-updates/blocks') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{ url('combined-updates/blocks') }}" style="color:#000; display:block; ">Blocks</a>
	                    </li>
	                    <li class="{{ Request::is('combined-updates/parkings') ? 'submenu-selected' : '' }}">
	                    	<a href="{{ url('combined-updates/parkings') }}"style="color:#000; display:block;">Parkings</a>
	                    </li>
	                    <li class="{{ Request::is('combined-updates/corridors') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{ url('combined-updates/corridors') }}" style="color:#000; display:block;">Corridors</a>
	                    </li>
	                    <li class="{{ Request::is('combined-updates/lifts') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{ url('combined-updates/lifts') }}" style="color:#000; display:block;">Lifts</a>
	                    </li>
    				</ul>
    			</div>
            </li>
            
            
            <li id="adminDD" class="collapsing-menu {{ Request::is('admin/*') ? 'selected-nav' : '' }}">
                <a href="#" 
                	data-toggle="collapse" 
                	data-parent="#mainMenu" 
                	data-target='#adminMenu' 
                	aria-controls='adminMenu' 
                	aria-expanded="false"
                	>Administration <b class="caret"></b>
                </a>
                <div class="collapse submenu" id="adminMenu">
	                <ul >
	                    <li class="{{ Request::is('admin/items/*') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{ url('/admin/items') }}" style="color:#000; display:block;">Manage Tracking items</a>
	                    </li>
	                    @ability('admin,superman', 'admin-user-read,admin-user-rw')
	                    <li class="{{ Request::is('admin/users*') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{ url('/admin/users') }}" style="color:#000; display:block;">Manage Users</a>
	                    </li>
	                    @endability
	                    
	                    @permission('admin-role-manage')
	                    <li class="{{ Request::is('admin/roles/*') ? 'submenu-selected' : '' }}" >
	                    	<a href="{{ url('/admin/roles')}}" style="color:#000; display:block;">Manage Roles</a>
	                    </li>
	                    @endcan
	                </ul>
                </div>
            </li>
            @endrole
            <li><a href="{{ url('/logout') }}"><span class="fa-md pull-right"><i class="fa fa-sign-out  "></i></span>Logout</a></li>
        @else
            <li><a href="{{ url('/login') }}">Login</a></li>
            <li>
                <a href="#">About</a>
            </li>
        @endif
    </ul>
    
    </div>
    
</nav>

<div class="navbar navbar-default navbar-fixed-top navbar-inverse">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#trackerNav" data-canvas="body">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navmenu-brand" href="#" style="padding:0;padding-top: 5px;font-family: 'Courgette', cursive;color:white;font-size: 27px;">{{config('app.my_app_name')}}</a>
</div>
</div>

<script>
    @if (Request::is('combined-updates/*'))
    	jQuery('#combinedUpdate').collapse();
    @endif
    @if (Request::is('home/*'))
    	jQuery('#individualUpdates').collapse();
    @endif
    @if (Request::is('admin/*'))
        jQuery('#adminMenu').collapse();
    @endif

    $(document).ready(function(){
    	  $("#experiment1").on("hide.bs.collapse", function(){
    		  $(this).parent().children('a').removeClass()
    	  });
    	  $("#experiment1").on("shown.bs.collapse", function(){
    		  $(this).parent().children('a').addClass("primary-menu-highlight");
    	  });
    	});
</script>