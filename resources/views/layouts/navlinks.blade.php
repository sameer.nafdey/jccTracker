<div class="row">
    <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{url('/home/flat')}}">Your flats</a></li>
<!--            <li><a href="{{url('/home/flat/other')}}">Other flats</a></li>-->
            <li><a href="{{url('/home/blocks')}}">Blocks</a></li>
            <li><a href="{{url('/home/corridor')}}">Corridors</a></li>
            <li><a href="{{url('/home/parking')}}">Parking Floors</a></li>
            <li><a href="{{url('/home/lift')}}">Lifts</a></li>
        </ol>
    </div>
</div>