<!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="attachedRolesModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Roles</h4>
			</div>
			<div class="modal-body">
				@if(isset($allRoles))
				<div class="list-group">
					@foreach($allRoles as $key=>$role)
					  	@if($role->name != 'superman')
						  	@if(isset($selectedUser))
							  	@if($role->name=='flatowner')
							  	<a href="#" class="list-group-item disabled" data-toggle="popover" data-trigger="hover" title="Help" data-placement="bottom" data-content="When flat is assigned to user, this role is attached.">
							  	@else
							  	<a href="#" class="list-group-item">
							  	@endif
						  		<!-- is the role flat owner? user has that role? then can't modify. -->
						  		@if($selectedUser->hasRole($role->name))
									<span class="fa fa-check" aria-hidden="true" style="float:right;font-size:30px;margin-top:5px;width:30px;"></span>
									<input type="checkbox" style="display: none" id="{{$role->name}}" name="roles[]" value="{{Crypt::encrypt($role->id)}}" checked="checked"/>
						  		@else
						  			<span class="fa fa-close" aria-hidden="true" style="float:right;font-size:30px;margin-top:5px;width:30px;"></span>
						  			<input type="checkbox" style="display: none" id="{{$role->name}}" name="roles[]" value="{{Crypt::encrypt($role->id)}}"/>
						  		@endif
						  	@else
						  	<a href="#" class="list-group-item">
						  		<span class="fa fa-close" aria-hidden="true" style="float:right;font-size:30px;margin-top:5px;width:30px;"></span>
						  		<input type="checkbox" style="display: none" id="{{$role->name}}" name="roles[]" value="{{Crypt::encrypt($role->id)}}"/>
						  	@endif
							    <h4 class="list-group-item-heading">{{$role->display_name}}</h4>
							    <p class="list-group-item-text">{{$role->description}}</p>
							    
						  	</a> <!-- open tag is conditioned above -->
					  @endif
				  @endforeach
				</div>
				@else
				<div>There is some problem, please contact administrator.</div>
				@endif
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="">Close</button>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
jQuery('.list-group-item').on('click', function(){

	var checkOrCrossImg = $(this).find('span');
	if(checkOrCrossImg){
		checkOrCrossImg.removeClass('fa-check text-primary');
		checkOrCrossImg.removeClass('fa-close');
	}
	
	var myRoleCheckbx = $(this).find('input');
	
	if(myRoleCheckbx.prop('checked')){
		myRoleCheckbx.prop('checked', false);
		checkOrCrossImg.addClass('fa-close');
	}else{
		myRoleCheckbx.prop('checked', true);
		checkOrCrossImg.addClass('fa-check text-primary');
	}

});
</script>