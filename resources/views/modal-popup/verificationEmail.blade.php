<!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="verificationEmailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send verification email</h4>
      </div>
      <div class="modal-body">
      	<input id="emailSubject" name="emailSubject" type="text" placeholder="Welcome" class="form-control" value="Welcome to {{config('app.my_app_name')}}!"/>
      	<br/>
      	<textarea id="emailContent" name="emailContent" data-provide="markdown" rows="10" class="form-control" placeholder="If you want to add additional message, type here. Click Preview."></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnVerifyPreview">Preview</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="previewVerificationEmailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Preview</h4>
      </div>
      <div class="modal-body">
      	<div id="previewBody"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="sendVerificationMail">Send</button>
      </div>
    </div>

  </div>
</div>