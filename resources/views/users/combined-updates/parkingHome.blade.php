@extends('layouts.app')
@section('title', '- Combined Updates - Flats')
@section('pageHeader')
<style>
    .item-description {
        color: #777;
        font-size: 80%;
        padding-bottom: 10px;
        border-bottom: 1px solid #eee;
        margin-bottom: 1%;
        margin-top: 0.3%
    }
    .item-table-heading{
        border-bottom: 1px dashed #eee;
        margin-bottom: 1.5%
    }
</style>

@endsection

@section('content')
<div class="container">
    
    <div class='row' style="margin-bottom: 5%">
        <div class="panel-group">
            <div id="mainPanel" class="panel panel-default">
                <div class="panel-body">
                    <div class='row'>
                        <div class='col-xs-12'>
                            <h3>Update multiple parking's status here. </h3>
                            <dl class="dl-horizontal col-xs-12">
                                <dt>Block & Item</dt>
                                <dd>Select Block & Item you want to update.</dd>
                                <dt>Status</dt>
                                <dd>Then select appropriate status on each parking floor.</dd>
                                <dt>Update</dt>
                                <dd>Click Save to record updated status.</dd>
                            </dl>
                        </div>
                        <div class='col-xs-12'>
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group btn-group-lg" style="width:100%">
                                    <button id="btn-block-select" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:100%">
                                        Select Block <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" style="width:100%">
                                        @foreach($allBlocks as $block)
                                        <li><a href="#" onclick="selectBlock({{$block->id}}); return false;">{{$block->name}} Block</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="select-item-div" class="col-xs-12" style="margin-top: 1%;display: none;">
                            <div class="list-group">
                                @foreach($allItemDefinitions as $key=>$def)
                                <a href="#" class="list-group-item" onclick="selectItem({{$def->id}}, '{{$def->title}}'); return false;">
                                    <h4 class="list-group-item-heading">{{$def->title}}<span class="glyphicon glyphicon-chevron-right pull-right"></span></h4>
                                    <p class="list-group-item-text">{{$def->description}}</p>
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div id="trackingPanel" class='panel panel-default' style="display:none" >
                
            </div>

        </div>

    </div>
</div>
@include('layouts.shareddata')
@endsection
@section('pageJs')
<script src="/js/users/combined-updates/parking.js" type="text/javascript"></script>
@endsection
