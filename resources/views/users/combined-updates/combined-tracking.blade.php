<div class="panel-body">
    <div class="row">
        <div class="col-xs-12">
            <blockquote>
                Select the current status of "<span id="itemLabel">{{$itemDef->title}}</span>" on each {{$item_type}} and click save at bottom.
                <footer>
                    <span class="glyphicon glyphicon-ok"></span> states current status.
                </footer>
                <footer>Date below respective status shown when it was set last.</footer>
            </blockquote>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="progress" style="display: none">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    0%
                </div>
            </div>
        </div>

    </div>
    <?php $completeCount = 0; ?>
    @foreach($allEntities as $entity)
    <?php
    $item = App\Model\EntityItem::where('entity_id', $entity->id)->where('entity_type', $item_type)->where('item_def_id', $itemDef->id)->first();
    if ($item) {
        $itemStatus = $item->entity_item_statuses()->orderBy('recorded_at', 'desc')->first();
    } else {
        unset($itemStatus);
    }

    if (isset($itemStatus) && $itemStatus->status === 'COMPLETED') {
        $completeCount++;
    }

    
    ?>
    
    <div class="row visible-xs-block" style=" border-top: 1px;border-style: dashed;border-left: 0px;border-bottom: 0px;border-right: 0px;border-color: #ccc;">
	    <div class="col-xs-12">
	    	<h4 >
	    		@if($entity instanceof App\Model\Corridor)Floor-{{$entity->floor}}@endif
	            @if($entity instanceof App\Model\Flat){{$entity->getReadableName()}} @endif
	            @if($entity instanceof App\Model\Block)Block-{{$entity->name}} @endif
	            @if($entity instanceof App\Model\Parking){{$entity->floor}} @endif
	            @if($entity instanceof App\Model\Lift)L-{{$entity->liftnumber}} @endif
	    	</h4>
	    </div>
    	
    </div>
    
    <div class="row visible-xs-block" style=" padding-bottom:8px;">
    	<div class="col-xs-12">
    		<div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default">
                    <input name="btn-status-{{$entity->id}}" type="radio" autocomplete="off" value='NOTSTARTED' data-value='{{$entity->id}}'>
                    @if(isset($itemStatus) && $itemStatus->status==='NOTSTARTED')
                    <span class="glyphicon glyphicon-ok"></span>
                    Not Started
                    <br><small>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $itemStatus->recorded_at)->toFormattedDateString()}}</small>
                    @else
                    Not Started
                    @endif
                    
                </label>
                <label class="btn btn-default">
                    <input name="btn-status-{{$entity->id}}" type="radio" autocomplete="off" value='INPROGRESS' data-value='{{$entity->id}}'>
                    @if(isset($itemStatus) && $itemStatus->status==='INPROGRESS')
                    <span class="glyphicon glyphicon-ok"></span>
                    In Progress
                    <br><small>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $itemStatus->recorded_at)->toFormattedDateString()}}</small>
                    @else
                    In Progress
                    @endif
                    
                </label>
                <label class="btn btn-default">
                    <input name="btn-status-{{$entity->id}}" type="radio" autocomplete="off" value='COMPLETED' data-value='{{$entity->id}}'>
                    @if(isset($itemStatus) && $itemStatus->status==='COMPLETED')
                    <span class="glyphicon glyphicon-ok"></span>
                    Completed
                    <br><small>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $itemStatus->recorded_at)->toFormattedDateString()}}</small>
                    @else
                    Completed
                    @endif
                    
                </label>
            </div>
    	</div>
    </div>
    
    <div class="row hidden-xs" style="margin-top:1%; margin-bottom:1%; position:relative">
        <div class="col-xs-2" style="position: absolute;top: 50%;transform: translateY(-50%);">
            <h4>
                <span class="label label-default">
                @if($entity instanceof App\Model\Corridor)Flr-{{$entity->floor}}@endif
                @if($entity instanceof App\Model\Flat){{$entity->getReadableName()}} @endif
                @if($entity instanceof App\Model\Block)Blck-{{$entity->name}} @endif
                @if($entity instanceof App\Model\Parking){{$entity->floor}} @endif
                @if($entity instanceof App\Model\Lift)L-{{$entity->liftnumber}} @endif
                </span>
            </h4>
        </div>
        <div class='col-xs-10 col-xs-offset-2'>
            <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default">
                    <input name="btn-status-{{$entity->id}}" type="radio" autocomplete="off" value='NOTSTARTED' data-value='{{$entity->id}}'>
                    @if(isset($itemStatus) && $itemStatus->status==='NOTSTARTED')
                    <span class="glyphicon glyphicon-ok"></span>
                    Not Started
                    <br><small>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $itemStatus->recorded_at)->toFormattedDateString()}}</small>
                    @else
                    Not Started
                    @endif
                    </input>
                </label>
                <label class="btn btn-default">
                    <input name="btn-status-{{$entity->id}}" type="radio" autocomplete="off" value='INPROGRESS' data-value='{{$entity->id}}'>
                    @if(isset($itemStatus) && $itemStatus->status==='INPROGRESS')
                    <span class="glyphicon glyphicon-ok"></span>
                    In Progress
                    <br><small>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $itemStatus->recorded_at)->toFormattedDateString()}}</small>
                    @else
                    In Progress
                    @endif
                    </input>
                </label>
                <label class="btn btn-default">
                    <input name="btn-status-{{$entity->id}}" type="radio" autocomplete="off" value='COMPLETED' data-value='{{$entity->id}}'>
                    @if(isset($itemStatus) && $itemStatus->status==='COMPLETED')
                    <span class="glyphicon glyphicon-ok"></span>
                    Completed
                    <br><small>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $itemStatus->recorded_at)->toFormattedDateString()}}</small>
                    @else
                    Completed
                    @endif
                    </input>
                </label>
            </div>
        </div>
    </div>
    @endforeach
    <div class="row">
        <div class="btn-group btn-group-justified" role="group" aria-label="..." style="margin-top:5px; margin-bottom: 5px;padding-left: 5px;padding-right: 5px;">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary" onclick="save();
                            return false;">Save</button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-default active" onclick="reset();
                                    return false;">Cancel</button>
            </div>
        </div>
    </div>


    <?php
    $completionPercentage = ($completeCount / count($allEntities)) * 100;
    ?>
    <script>
                var completionPercentage = Math.ceil({{$completionPercentage}});
    </script>
</div>