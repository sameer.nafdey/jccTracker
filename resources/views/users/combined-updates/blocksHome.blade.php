@extends('layouts.app') @section('title', '- Combined Updates - Flats')
@section('pageHeader')
<style>
.item-description {
	color: #777;
	font-size: 80%;
	padding-bottom: 10px;
	border-bottom: 1px solid #eee;
	margin-bottom: 1%;
	margin-top: 0.3%
}

.item-table-heading {
	border-bottom: 1px dashed #eee;
	margin-bottom: 1.5%
}
</style>

@endsection @section('content')
<div class="container">
	<div id="mainPanel" class='row' style="margin-bottom: 5%">
		<div class='col-xs-11'>
			<h3>Update multiple block's status here.</h3>
			<dl class="dl-horizontal col-xs-12">
				<dt>Item</dt>
				<dd>Select Item you want to update.</dd>
				<dt>Status</dt>
				<dd>Then select appropriate status on each block.</dd>
				<dt>Update</dt>
				<dd>Click Save to record updated status.</dd>
			</dl>
		</div>
	</div>
	<div class="row">
	<div id="select-item-div" class="col-xs-12" style="margin-top: 1%;">
		<div class="list-group">
			@foreach($allItemDefinitions as $key=>$def) <a href="#"
				class="list-group-item"
				onclick="selectItem({{$def->id}}, '{{$def->title}}'); return false;">
				<h4 class="list-group-item-heading">
					{{$def->title}}<span
						class="glyphicon glyphicon-chevron-right pull-right"></span>
				</h4>
				<p class="list-group-item-text">{{$def->description}}</p>
			</a> @endforeach
		</div>
	</div>
	</div>
	<div class="row">
	<div id="trackingPanel" class='panel panel-default'
		style="display: none"></div>
		</div>
</div>
</div>
@include('layouts.shareddata') @endsection @section('pageJs')
<script src="/js/users/combined-updates/blocks.js"
	type="text/javascript"></script>
@endsection
