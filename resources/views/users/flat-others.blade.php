@extends('layouts.app')
@section('title', '- Other flat')
@section('pageHeader')
<style>
    .item-description {
        color: #777;
        font-size: 80%;
        padding-bottom: 10px;
        border-bottom: 1px solid #eee;
        margin-bottom: 1%;
        margin-top: 0.3%
    }
    .item-table-heading{
        border-bottom: 1px dashed #eee;
        margin-bottom: 1.5%
    }
</style>
@endsection

@section('content')
<div class="container">
    @include('layouts.navlinks')
    <div class='row' style="margin-bottom: 5%">
        @if(isset($ownedflats))
        @foreach($ownedflats as $key =>$flat)
        @include('users.sections.flat')
        @endforeach
        @endif
    </div>
</div>
@endsection
@section('pageJs')
<script src="/js/users/flat.js" type="text/javascript"></script>
@endsection
