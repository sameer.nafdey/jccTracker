@extends('layouts.app')
@section('title', '- Corridors')
@section('pageHeader')
<style>
    .item-description {
        color: #777;
        font-size: 80%;
        padding-bottom: 10px;
        border-bottom: 1px solid #eee;
        margin-bottom: 1%;
        margin-top: 0.3%
    }
    .item-table-heading{
        border-bottom: 1px dashed #eee;
        margin-bottom: 1.5%
    }
</style>
@endsection

@section('content')
<div class="container">
    <h3>Corridor</h3>
    
    <div class='row' style="margin-bottom: 5%">
        <div class="col-xs-12">
            <div class="panel panel-default" >
                <div class="panel-heading">
                    
                    <div id="div-corridor-selection" style="text-align: right; margin-left: 5px;" class="pull-right">
                        @include('users.sections.corridor-dropdown')
                    </div>
                    <div style="text-align: right;" class="pull-right">
                        <div class="btn-group">
                            <button id="btn-block-select" type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$selectedBlock->name}} Block <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                @foreach($allBlocks as $block)
                                <li><a href="#" onclick="pullCorridorsForBlock('{{$block->id}}', '{{$block->name}}'); return false;">{{$block->name}} Block</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div>Corridor Status
                        @if(isset($selectedBlock))
                        - <strong id="selectedEntityTitle">{{$selectedBlock->name}} Block - {{$selectedCorridor->floor}} Floor</strong>
                        @endif
                    </div>
                    <div style="color:#888;">Itemized recorded status is listed below. You can also update status here. Remember to click save at bottom.</div>
                </div>

                @include('users.sections.corridor')
            </div>
        </div>
    </div>
</div>
@endsection
@section('pageJs')
<script src="/js/users/tracking-common.js" type="text/javascript"></script>
<script src="/js/users/corridor.js" type="text/javascript"></script>
@endsection
