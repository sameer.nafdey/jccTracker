
@if(isset($allLiftInSelectedBlock))
<div class="btn-group">
    <button id="btn-lift-select" type="button" class="my-tooltip my-dropdown btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title='Select a lift' data-placement="top">
        @if(isset($selectedLift))
        Lift {{$selectedLift->liftnumber}}
        @else
        Select Lift
        @endif
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        @foreach($allLiftInSelectedBlock as $lift)
        <li><a href="#" onclick="selectLiftForUpdate('{{$selectedBlock->id}}', '{{$lift->liftnumber}}'); return false;">Lift {{$lift->liftnumber}}</a></li>
        @endforeach
    </ul>
</div>
@endif