
@if(isset($allCorridorsInSelectedBlock))
<div class="btn-group">
    <button id="btn-corridor-select" type="button" class="my-tooltip my-dropdown btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title='Now select a floor' data-placement="top">
        @if(isset($selectedCorridor))
        Floor {{$selectedCorridor->floor}}
        @else
        Select Floor
        @endif
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        @foreach($allCorridorsInSelectedBlock as $corridor)
        <li><a href="#" onclick="selectCorridorForUpdate('{{$selectedBlock->id}}', '{{$corridor->floor}}'); return false;">{{$corridor->floor}} Floor</a></li>
        @endforeach
    </ul>
</div>
@endif