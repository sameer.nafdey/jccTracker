<div id="panelBodyForTrackingItem" class="panel-body">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="progress" style="display: none">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    0%
                </div>
            </div>
        </div>

    </div>
    
    @if(isset($selectedLift) && isset($allLiftItemDefinitions) && count($allLiftItemDefinitions)>0)

    <div class="row item-table-heading">
        <div class="col-xs-5"><h4>ITEM</h4></div>
        <div class="col-xs-3"><h4>STATUS</h4></div>
        <div class="col-xs-4" style="text-align: right"><h4>ACTION</h4></div>
    </div>
    <?php $completeCount = 0; ?>
    @foreach($allLiftItemDefinitions as $key=>$def)
    <?php
    $allEntityItem = App\Model\EntityItem::where('item_def_id', $def->id)->where('entity_id', $selectedLift->id)->first();
    $statusObj = '';

    if ($allEntityItem) {
        $statusObj = App\Model\EntityItemStatus::where('entity_item_id', $allEntityItem->id)->orderBy('recorded_at', 'desc')->first();
    }
    if ($statusObj) {
        if ($statusObj->status === 'COMPLETED') {
            $completeCount++;
        }
    }
    ?>
    <div class="row">
        <div class="col-xs-5">
            {{$def->title}}
        </div>
        <div class="col-xs-3">
            @if($statusObj)
            {{$statusObj->status==='INPROGRESS'?'In Progress':($statusObj->status==='NOTSTARTED'?'Not Started':($statusObj->status==='COMPLETED'?'Completed':''))}}
            @else
            Not available
            @endif
        </div>
        <div class="col-xs-4" style="text-align: right">
            <div class="btn-group">
                <button id="btn-item-{{$def->id}}" type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Update status <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="setStatus({{$def->id}}, {{$selectedLift->id}}, 'NOTSTARTED'); return false;">Not Started</a></li>
                    <li><a href="#" onclick="setStatus({{$def->id}}, {{$selectedLift->id}}, 'INPROGRESS'); return false;">In Progress</a></li>
                    <li><a href="#" onclick="setStatus({{$def->id}}, {{$selectedLift->id}}, 'COMPLETED'); return false;">Completed</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row item-description">
        <div class="col-xs-5">
            {{$def->description}}
        </div>
        <div class="col-xs-7">
            @if($statusObj)
            Updated : {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $statusObj->recorded_at)->toDayDateTimeString()}}
            @endif
        </div>
    </div>
    @endforeach

    <?php
    $completionPercentage = ($completeCount / count($allLiftItemDefinitions)) * 100;
    ?>
    <script>
                                var completionPercentage = Math.ceil({{$completionPercentage}});</script>
    <div style="margin-top:2%">
        <button class="btn btn-primary" style="width: 100%" onclick="save('/track/lift/update',{{$selectedLift->block_id}}, '{{$selectedLift->liftnumber}}', 'lift');">Save</button>
    </div>

    @else
    <strong>No records available.</strong>
    @endif

</div>

<div id="panelBodyNoTrackingItem" class="panel-body" style="display: none">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h4>Select lift to continue.</h4>
        </div>
    </div>
</div>