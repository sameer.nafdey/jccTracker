<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style type="text/css">
			body{
				font-family: Century Gothic, Lucida Grande, Trebuchet MS;
			}
			
			#emailFooter{
				font-size: small;
			}

		</style>
    </head>
    <body>
    	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
		    <tr>
		        <td align="center" valign="top">
		            <table border="0" cellpadding="20" cellspacing="0"  id="emailContainer">
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
		                            <tr>
		                                <td align="center" valign="top">
		                                    <h3>Forgot your password?</h3>
		                                </td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailBody">
		                            <tr>
		                                <td align="left" valign="top">
		                                     <div>
									            As we received request to reset your password, we're sending you a secure link to do just that.
									            <br/>
									            <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">Click here</a><br/>
									        </div>
		                                </td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="thankYou">
		                            <tr>
		                                <td align="left" valign="top">
		                                    Thank you<br />JCCOA
		                                    <br/>
		                                    <a href="{{URL::to('/')}}" target="_blank" style="font-size:small">{{URL::to('/')}}</a>
		                                </td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailFooter">
		                            <tr>
		                                <td align="left" valign="top">
		                                    If you wish to opt out of the application, kindly send email to <a href="mailto:admin@jcctracker.samify.com">admin@jcctracker.samify.com</a>.
		                                </td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		            </table>
		        </td>
		    </tr>
		</table>
    </body>
</html>