@extends('layouts.app') 
@section('title', 'Signup') 
@section('pageHeader')
<link href="/css/select2.min.css" rel="stylesheet" />
<link href="/css/bootstrap2-toggle.min.css" rel="stylesheet" />
<style type="text/css">
/*** custom checkboxes ***/

input[type=checkbox] { display:none; } /* to hide the checkbox itself */
input[type=checkbox] + label:before {
  font-family: FontAwesome;
  display: inline-block;
  font-size:20px; margin-top:-5px; margin-bottom:0px;
}

input[type=checkbox] + label:before { content: "\f096"; } /* unchecked icon */
input[type=checkbox] + label:before { letter-spacing: 0px;font-size:20px; margin-top:0px; } /* space between checkbox and label */

input[type=checkbox]:checked + label:before { content: "\f00c"; } /* checked icon */
input[type=checkbox]:checked + label:before { letter-spacing: 0px; margin-right: -4px;} /* allow space for check mark */
</style>
@endsection
@section('content')
@include('layouts.shareddata')
<div class="container">
@if(Request::path() =='admin/users/register') 			
{{ 
	Form::open(array('url' => 'admin/users')) 
}}  
@else
{{ 
	Form::open(array('url' => ['admin/users', '0']))
	
}}
{{method_field('PUT') }}
@endif

@if(count($errors))
<div class="row" style="margin-top: 10px;">
	<div class="col-sm-8 col-sm-offset-2">	
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			 @endforeach
		</ul>
	</div>
</div>
@endif


	<div class="row" style="margin-top: 10px;">
		
		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			<label for="name" class="col-md-4 control-label">Name</label>

			<div class="col-md-6">
				<input id="name" type="text" class="form-control" name="name"
					value="{{ isset($selectedUser) ? $selectedUser->name : '' }}">
					@if ($errors->has('name')) <span
					class="help-block"> <strong>{{ $errors->first('name') }}</strong>
				</span> @endif
			</div>
		</div>
	</div>
	
	<div class="row" style="margin-top: 10px;">
		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			<label for="email" class="col-md-4 control-label">E-Mail Address</label>

			<div class="col-md-6">
				<input id="email" type="email" class="form-control" name="email"
					value="{{ isset($selectedUser) ? $selectedUser->email : ''}}" {{ isset($selectedUser) ? 'disabled="true"' : ''}}> @if ($errors->has('email')) <span
					class="help-block"> <strong>{{ $errors->first('email') }}</strong>
				</span> @endif
			</div>
		</div>
	</div>
	<div class="row" style="margin-top: 10px;">
		
		<div class="form-group{{ $errors->has('flatDetailsNeeded') ? ' has-error' : '' }}">
			<label for="flatId" class="col-md-4 control-label">Flat owner</label>
			
			<div class="col-md-6" data-toggle="tooltip" title="Select flat">
				<div class="input-group flat-selection-group" style="background-color: #eee;border: 1px solid #ccc;border-radius: 4px;">
					<span class="input-group-addon" style="border:0px;padding-bottom: 0px;padding-top: 8px;">
				        
				        <input id="isFlatOwner" name="isFlatOwner" type="hidden" value="false">
						<input id="flatOwnerCheck" name="flatOwnerCheck" type="checkbox" value="false">
						<label for="flatOwnerCheck"></label>
				    </span>
					<select id='flatId' name='flatId' class="js-flat-data-array js-states form-control"></select> 
					
				</div>
				@if ($errors->has('flatDetailsNeeded')) 
				<span class="help-block"> <strong>{{ $errors->first('flatDetailsNeeded') }}</strong></span> 
				@endif
			</div>
		</div>				  
	</div>
	
	<div class="row" style="margin-top: 10px;">
		<div class="form-group{{ $errors->has('isActivated') ? ' has-error' : '' }}">
			<label for="isActivated" class="col-md-4 control-label">Account activation status</label>

			<div class="col-md-6">
				{{ Form::hidden('isActivated', 'false', array('id'=>'isActivated', 'name'=>'isActivated')) }}
				<input id="isActivated" name="isActivated" class="form-control btn btn-lg" type="checkbox" data-toggle="toggle" data-on="Activated" data-off="Deactivated" data-width="100%" data-height="34px" {{isset($selectedUser) && $selectedUser->enabled? "checked":""}}/>
					@if ($errors->has('isActivated')) 
					<span class="help-block"> <strong>{{ $errors->first('isActivated') }}</strong></span> 
					@endif
			</div>
		</div>
	</div>


	@if(isset($selectedUser))
	<div class="row" style="margin-top: 10px;">
		<div class="form-group{{ $errors->has('isActivated') ? ' has-error' : '' }}">
			<label for="isConfirmed" class="col-md-4 control-label">Email verification status</label>
			<div class="col-md-6">
				@if($selectedUser->confirmed==1)
				<h3 id="isConfirmed" style="margin-top: 0px;"><span class="label label-success" data-toggle="popover" data-trigger="hover" title="Help" data-content="User has verified his/her primary email address." data-placement="right">Confirmed</span></h3>
				@else
				<h3 id="isConfirmed" style="margin-top: 0px;"><span class="label label-danger" data-toggle="popover" data-trigger="hover" title="Help" data-content="User has NOT verified his/her primary email address. You can send verification email again." data-placement="right">Not Confirmed</span>
					<a href="#" class="btn btn-primary btn-md pull-right" data-toggle="modal" data-target="#verificationEmailModal">Send verification email</a></h3>
				@endif
			</div>
		</div>
	</div>
	@endif

	@if(Request::path() !='admin/users/register') 
	<div class="row" style="margin-top: 10px;">
		<div class="form-group">
			<label for="userRoles" class="col-md-4 control-label">Roles</label>
			<div class="col-md-6">
				
				<h3 id="userRoles" style="margin-top: 0px;">
					<a href="#" class="btn btn-primary btn-md " data-toggle="modal" data-target="#attachedRolesModal">View roles</a></h3>
				
			</div>
		</div>
	</div>
	@endif
	
	<div class="row" style="margin-top: 10px;">
		
		<div class="form-group" style="margin-bottom: 50px;">
			<div class="col-md-6 col-md-offset-4">
				<div class="btn-group btn-group-justified" role="group" aria-label="..." style="margin-top:5px; margin-bottom: 5px;padding-left: 5px;padding-right: 5px;">
            		<div class="btn-group" role="group">
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-btn fa-user"></i> {{ isset($selectedUser) ? 'Update' : 'Enroll'}}
						</button>
					</div>
					<div class="btn-group" role="group">
						<a href="{{url('/admin/users')}}" class="btn btn-default active" >Cancel</a>
					</div>
			</div>
		</div>
		
	</div>
	
	
	<div style="display: none">
		<input type="text" id="id" name="id" value="{{isset($selectedUser)?Crypt::encrypt($selectedUser->id):''}}" />
	</div>
	
	@include('modal-popup.attached-roles')
{{Form::close()}}
</div>
@include('modal-popup.verificationEmail')

@endsection 
@section('pageJs')
<script src="/js/admin/homepage.js" type="text/javascript"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/bootstrap2-toggle.min.js"></script>
<script type="text/javascript">
	var allFlatData = {!!$allFlats!!};

	var flatDropdown = $(".js-flat-data-array").select2({
		  data: allFlatData,
		  placeholder: "Select a flat",
		  allowClear: true
		});

	flatDropdown.on("select2:select", function (e){
			var sel = e.params.data;
			if(sel){ 
				console.log("before");
				console.log(selectedFlatID);
				selectedFlatID = sel["id"];
				$('.select2-selection__clear').css("width", "30px").css("font-size", "27px").css("padding-left", "9px");
				console.log(selectedFlatID);
			}
	});


	flatDropdown.on("select2:unselecting", function (e) {
        selectedFlatID = '';
    });
    
	flatDropdown.on("select2:open", function() {
    	$(".select2-search__field").attr("placeholder", "Search.. e.g. H-604");
	});
	flatDropdown.on("select2:close", function() {
    	$(".select2-search__field").attr("placeholder", null);
	});

	var toggleFlatSelect = function () {
	    if ($("input[type='checkbox'][name='flatOwnerCheck']").is(":checked")) {
	        $('#flatId').prop('disabled', false);
	    }
	    else {
	        $('#flatId').prop('disabled', 'disabled');
	        $("select").val('').change();
	    }
	  };
	  $(toggleFlatSelect);
	  $("input[type='checkbox'][name='flatOwnerCheck']").change(toggleFlatSelect);
	  $("select").val('').change();

	  $('#flatOwnerCheck').change(function() {
		   console.log($(this).is(':checked'));
	        $('#isFlatOwner').val($(this).is(':checked'));        
	    });

	  var selectedFlatID = "";

	  @if(isset($ownerFlatID) && $ownerFlatID!=null)
		 selectedFlatID = '{{$ownerFlatID}}';
		 if(selectedFlatID){
			 $("input[type='checkbox'][name='flatOwnerCheck']").prop('checked', true);
			 $('select').val(selectedFlatID).change();
		 }
	  @endif

	  $(document).on('submit','.form-horizontal',function(){
		   console.log("Form submission event");
		   if ($("input[type='checkbox'][name='flatOwnerCheck']").is(":checked") && !selectedFlatID) {
			   
			   $('.flat-selection-group').attr('style', "border-radius: 5px; border:#FF0000 1px solid;");
			   return false;
		   }else{
			   $('.flat-selection-group').attr('style', "");
			   
			   return true;
		   }
		   
		});

	  	$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip(); 
		    //$('#isActivated').bootstrapToggle();
		    $("input[type='checkbox'][name='isActivated']").bootstrapToggle();
		    $('[data-toggle="popover"]').popover(); 

		    $('.select2-selection').css("height", "34px").css("padding-top", "4px");
		    $('.select2-selection__clear').css("width", "30px").css("font-size", "27px").css("padding-left", "9px");
		    
		});

	  	
	  	@if(isset($selectedUser))
		$('#btnVerifyPreview').click(function(event){
			event.preventDefault();
			var requestObject = new Object();
	  	    requestObject['userId'] = {{ isset($selectedUser) ? $selectedUser->id : "''"}};

	  	    requestObject['mailSubject'] = $('#emailSubject').val();
	  	    requestObject['additionalMessage'] = $('#emailContent').val();
	  	    
	  	    //jccTrackerApp.showPleaseWait();
	  	    jQuery.get('/api/verifyemail/preview', requestObject, function (data) {

	  	        
	  	        if (data) {
	  	            
	  	        	$('#previewBody').html(data);
	  	        	$('#previewVerificationEmailModal').modal();
	  	            
	  	        }

	  	    }).fail(function (e) {
	  	        console.log('error' + e);
	  	    }).always(function () {
	  	        //jccTrackerApp.hidePleaseWait();
	  	    });
		});

		$('#sendVerificationMail').click(function(event){
			event.preventDefault();
			var requestObject = new Object();
	  	    requestObject['userId'] = {{ isset($selectedUser) ? $selectedUser->id : "''"}};

	  	    requestObject['mailSubject'] = $('#emailSubject').val();
	  	    requestObject['additionalMessage'] = $('#emailContent').val();
	  	    
	  	    //jccTrackerApp.showPleaseWait();
	  	    jQuery.post('/api/verifyemail/send', requestObject, function (data) {

	  	        
	  	        if (data) {
	  	            
	  	        	
	  	            
	  	        }

	  	    }).fail(function (e) {
	  	        console.log('error' + e);
	  	    }).always(function () {
	  	        //jccTrackerApp.hidePleaseWait();
	  	    });
		});
		@endif

</script>
@endsection
