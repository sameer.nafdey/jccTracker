@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Jain's Carlton Creek - Status tracker <small style="color: #777">- @Khajaguda, Hyderabad</small></div>

                <div class="panel-body">
                    <h3>Track & Monitor Status</h3>
<!--                    <ul>
                        <li>Monitor latest status of flat(s).</li>
                        <li>Update flats/blocks/corridor/parking/lift etc status.</li>
                        <li>Report your registration status, livability conditions in block etc.</li>
                        <li>Data collected here will be used to make reports and keep a track of the progress.</li>
                    </ul>-->
                    <p>Login to start tracking & monitoring progress.</p>
<!--                    <p>Once registered do not forget to verify your email address for authentication purpose.</p>-->
<!--                    <p>If already registered, Login and start using this app right away.</p>-->
                    <div style="margin-bottom: 10px">
                        <a href="{{ url('/login') }}" class='btn btn-primary'>Login</a>
                    </div>
                    <div>
                        <blockquote>
                            <footer>Disclaimer: {{config('app.my_app_name')}} is in Beta. There are no guarantee of anything.<br/>The data collected/shown on this site may have errors, so please go ahead make corrections if required. This site in no way has any tie-up with Jains Housing & Construction Ltd.</footer>
                            <footer>This is just a small step towards better tracking and transparency.</footer>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
