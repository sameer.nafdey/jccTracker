@extends('layouts.app')
@section('title', 'Admin panel')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id='panelItemManagement' style="margin-left:1%; margin-right:1%">
                <div class="row">
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Heads up!</strong> First select type then add title & short description click add.
                    </div>

                    <div id="warningAlert" class="alert alert-warning alert-dismissible" role="alert" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Warning!</strong> All fields are required.
                    </div>


                    <div class="form-inline">
                        <div class="form-group col-md-2">
                            <div class="btn-group">
                                <button id="btnDropDownItemType" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select <span class="caret"></span></button>

                                <ul class="dropdown-menu">
                                    <li><a href="#" onClick="selectItemType('block');
                                                    return false;">Block</a></li>
                                    <li><a href="#" onCLick="selectItemType('flat');
                                                    return false;">Flat</a></li>
                                    <li><a href="#" onClick="selectItemType('corridor');
                                                    return false;">Corridor</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#" onClick="selectItemType('lift');
                                                    return false;">Lift</a></li>
                                    <li><a href="#" onClick="selectItemType('parking');
                                                    return false;">Parking</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="entityItemTitle">Title: </label>
                            <input id="entityItemTitle"type="text" class="form-control" placeholder="Title" aria-describedby="basic-addon1">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="entityItemDesc">Description: </label>
                            <input id="entityItemDesc" type="text" class="form-control" placeholder="Description" aria-describedby="basic-addon1">
                        </div>
                        <div class="form-group col-md-2">
                            <button id="btnEntityItemDefSave" type="button" class="btn btn-primary" onclick="saveItemDef();
                                            return true;">Add</button>
                            <div id="editButtons" style="display: none">
                                <button id="btnEntityItemDefUpdate" type="button" class="btn btn-primary" onclick="updateItemDefinition();
                                                return true;">Update</button>
                                <a href="#" class="btn btn-md" onclick="cancelEdit()">
                                    <span class="glyphicon glyphicon-remove"></span> 
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row" style="margin-top:3%; margin-bottom:3%;">
                    <div class="col-md-12">
                        @if(isset($definitionsByType))
                        {{$definitionsByType}}
                        <div id="defListPlaceholder">

                        </div>
                        @else
                        <div id="defListPlaceholder"></div>
                        @endif
                    </div>
                </div>

            </div>

            @can('manage-user-roles')
            <div id="panelUserManagement" style="display:none;margin-left:1%; margin-right:1%">
                <div class="row" style="margin-top:3%; margin-bottom:3%;">
                    <div class="col-md-12">

                        <div id="userListPlaceholder">

                        </div>

                    </div>
                </div>
            </div>
            @endcan

        </div>
    </div>
</div>
@include('layouts.shareddata')
@endsection
@section('pageJs')
<script src="/js/admin/items.js" type="text/javascript"></script>
@endsection