@extends('layouts.app')
@section('title', 'Admin panel')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">

            <table class="table table-striped table-hover">
                @if(isset($usersList) && count($usersList)>0)
                <tr>
                    <th>#</th><th>Name</th><th>Email</th><th>Roles</th>
                </tr>

                @foreach($usersList as $key=>$usr)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$usr->name}}</td>
                    <td>{{$usr->email}}</td>
                    <td>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary active">
                                <input type="checkbox" autocomplete="off" >Admin
                            </label>
                            <label class="btn btn-primary">
                                <input type="checkbox" autocomplete="off">Customer
                            </label>
                        </div>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>

        </div>
    </div>
</div>