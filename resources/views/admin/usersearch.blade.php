@extends('layouts.app') @section('title', 'Manage users')
@section('pageheader')
<link rel="stylesheet" type="text/css"
	href="/css/DataTables/datatables.min.css" />

@endsection @section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-8 col-md-10">

			<h3>Users</h3>

		</div>
		<div class="col-xs-4 col-md-2">
			<h3>
				<a href="{{url('admin/users/register')}}" class="btn btn-primary btn-block">Add new</a>
			</h3>

		</div>
	</div>
	@if(isset($allUsers) && $allUsers->count()>0)
	
	<div class="row">
		<div class="col-sm-12">
			<table id="userList" class="display table table-striped responsive"
				cellspacing="0">
				<thead>
					<tr>
						<th scope="row" style="display:none">UserID</th>
						<th scope="row" class="visible-md visible-lg">#</th>
						<th>Flat</th>
						<th>Name</th>
						<th class="visible-md visible-lg">Phone</th>
						<th class="visible-md visible-lg">Email</th>
						<th scope="row" style="display: none">FlatID</th>
						<th>Status</th>
					</tr>
				</thead>

				<tbody>
					@foreach($allUsers as $key=>$user)
					@if($user->Name != "Superman")
					<tr>
						<td style="display: none">{{Crypt::encrypt($user->Id)}}</td>
						<td scope="row" class="visible-md visible-lg">{{(++$key)}}</td>
						@if($user->FlatID!=null)
						<td>{{$user->Block}}{{$user->Floor}}{{$user->Unit<10?'0'.$user->Unit:$user->Unit}}</td>
						@else
						<td>-NA-</td>
						@endif
						<td>{{$user->Name}}</td>
						<td class="visible-md visible-lg">{{$user->PrimaryContactNo != ''
							?$user->PrimaryContactNo: "Not available"}}</td>
						<td class="visible-md visible-lg">{{$user->PrimaryEmail}}</td>
						<td style="display: none">{{Crypt::encrypt($user->FlatID)}}</td>
						<td>
						@if($user->Activated==1)
						<i class="fa fa-user" aria-hidden="true" data-toggle="tooltip" title="Account activated." style="color: #4cae4c;"></i>
						@else
						<i class="fa fa-user-times" aria-hidden="true" data-toggle="tooltip" title="Account is not activated yet." style="color:#d9534f;"></i>
						@endif
						
						@if($user->EmailVerified==1)
						<i class="fa fa-check-square" aria-hidden="true" data-toggle="tooltip" title="User has confirmed email address." style="color:#4cae4c;"></i>
						@else
						<i class="fa fa-exclamation-triangle" aria-hidden="true" data-toggle="tooltip" title="User has not confirmed email address." style="color:#d9534f;"></i>
						@endif
						
						@if($user->FlatID!=null)
						<i class="fa fa-home" aria-hidden="true" data-toggle="tooltip" title="User is flat owner." style="font-size:15px;"></i>
						@endif
						</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>
		</div>
		@else
		<div class="col-sm-12">Strange! No user exist.</div>
		@endif
		
		<div class="col-sm-12">
			<label>Legends:</label>
			<ul>
				<li><i class="fa fa-user" aria-hidden="true" style="color: #4cae4c;"></i> / <i class="fa fa-user-times" aria-hidden="true" style="color:#d9534f;"></i> - Account activation status</li>
				<li><i class="fa fa-check-square" aria-hidden="true" style="color:#4cae4c;"></i> / <i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#d9534f;"></i> - Email verification status</li>
				<li><i class="fa fa-home" aria-hidden="true" data-toggle="tooltip" title="User is flat owner." style="font-size:15px;"></i> - User is a flat owner</li>
			</ul>
		</div>
		
	</div>
</div>
@endsection @section('pageJs')
<script type="text/javascript" src="/js/DataTables/datatables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip(); 
	
    var table = $('#userList').DataTable({
        	"columnDefs" : [{
            		"targets" : 0,
            		"visible" : false,
            		"searchable" : false
            	},
            	{
            		"targets" : 6,
            		"visible" : false,
            		"searchable" : false
            	}],
            "order" : [[1, "asc"]]
        });

    $('[data-toggle="tooltip"]').on('click', function(e){
        e.preventDefault();
        return false;
        }); 
    
    $('#userList tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        console.log( 'You clicked on '+data[0]+'\'s row' );

        var editURL = '/admin/users/'+data[0]+'/edit';
        window.location.href = editURL;
    } );
} );
</script>
@endsection
