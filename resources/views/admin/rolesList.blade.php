
@extends('layouts.app')
@section('title', 'Manage Roles')
@section('content')
<div class="container">
    
    
    <div class="row">
        
        <div class="col-xs-8 col-md-10">
            <h3>Roles</h3>
        </div>
        <div class="col-xs-4 col-md-2">
            <h3>
                <a href="{{url('admin/roles/create')}}" rel="button" class="btn btn-primary btn-block">Add New</a>
            </h3>
            
        </div>
    </div>
    
    <div class="row">
        <div id="select-item-div" class="col-xs-12" style="margin-top: 1%;display: block;">
            <div class="list-group">
                @foreach($roles as $r)
                <a href="#" class="list-group-item" onclick="window.location.href = '{{ url('admin/roles/') }}/{{$r->id}}/edit'">
                    <h4 class="list-group-item-heading">{{$r->display_name}}<span class="glyphicon glyphicon-chevron-right pull-right"></span></h4>
                    <p class="list-group-item-text">{{$r->description}}</p>
                </a>
                @endforeach
            </div>
        </div>
    </div>
    
    
</div>