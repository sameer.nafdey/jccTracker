

<table class="table table-striped table-hover">
    @if(isset($definitionsByType) && count($definitionsByType)>0)
    <tr>
        <th>#</th><th>Title</th><th>Description</th><th>Actions</th>
    </tr>

    @foreach($definitionsByType as $key=>$def)
    <tr>
        <td>{{++$key}}</td>
        <td>{{$def->title}}</td>
        <td>{{$def->description}}</td>
        <td>
            <a href="#" class="btn btn-sm btn-default btn-danger" onclick="removeItemDefinition({{$def->id}})">
                <span class="glyphicon glyphicon-remove"></span> 
            </a>
            <a href="#" class="btn btn-sm btn-default btn-success" onclick="editItemDefinition({{$def->id}}, '{{$def->title}}', '{{$def->description}}')">
                <span class="glyphicon glyphicon-pencil"></span> 
            </a>
        </td>
    </tr>
    @endforeach
    @else
    <strong>No records created.</strong>
    @endif
</table>