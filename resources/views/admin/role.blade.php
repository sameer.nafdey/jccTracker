@extends('layouts.app')
@section('title', '- Work with roles')
@section('content')
<div class="container">

	@if(Request::path() =='admin/roles/create') 
	{{ 
		Form::open(array('url' => 'admin/roles')) 
	}} 
	@else 
	{{ 
		Form::open(array('url' => ['admin/roles', $selectedRole->id]))
		
	}} 
	{{method_field('PUT') }}
	@endif
	<div class="row">
		<div class="col-xs-8 col-md-10">
			@if(Request::path() =='admin/roles/create')
			<h3>Create new Role</h3>
			@else
			<h3>Modify a role</h3>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Name" id="roleName" name="roleName" value="{{isset($selectedRole)?$selectedRole->name:''}}">
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<textarea rows="3" class="form-control" placeholder="Description" id="roleDescription" name="roleDescription">{{isset($selectedRole)?$selectedRole->description:''}}</textarea>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="list-group">
				@foreach($allPermissions as $permission)
				<a href="#" class="list-group-item  {{(isset($selectedRole) && $selectedRole->hasPermission($permission->name)) ?'active':''}}" onclick="selectRadio(this, {{$permission->id}}); return false;" style="padding-top:0px;">
					<div class="checkbox">
						<label><input type="checkbox" id="{{$permission->id}}" name="permission[]" value="{{$permission->id}}" {{(isset($selectedRole) && $selectedRole->hasPermission($permission->name)) ?'checked':''}}><strong>{{$permission->display_name}}</strong></label>
					</div>
					<p class="list-group-item-text">{{$permission->description}}</p>
				</a>
				@endforeach
			</div>

		</div>
	</div>
	
	<div style="display: none">
		<input type='hidden' name="id" value="{{isset($selectedRole)?$selectedRole->id:''}}"/>
	</div>

	<div class="row" style="padding-bottom:25px;">
		<div class="btn-group col-xs-8 " role="group" aria-label="...">
			<div class="btn-group" role="group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			<div class="btn-group" role="group">
				<a href="{{url()->previous()}}" class="btn btn-default">Cancel</a>
			</div>
		</div>
		@if(Request::path() != 'admin/roles/create')
		<button type="button" class="btn btn-warning col-xs-3" data-toggle="modal" data-target="#deleteConfirmationModal">Delete</button>
		@endif		
	</div>
	{{ Form::close() }}
</div>


@if(isset($selectedRole))
<!-- Modal -->
<div id="deleteConfirmationModal" class="modal fade" role="dialog">
	{{Form::open(array('url' => ['admin/roles', $selectedRole->id]))}}
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Please confirm</h4>
			</div>
			<div class="modal-body">
				<p>Decesion to remove an existing role needs a careful
					consideration. If you delete a role that is already assigned to
					users, will affect them and you will need to manually create
					and assign new roles to each of them.<br/> Are you sure?</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger">
					Delete
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
	{{method_field('DELETE') }}
	{{Form::close() }}
</div>
@endif
@endsection
@section('pageJs')
<script>
	function selectRadio(row, id){
		if(document.getElementById(id).checked){
			document.getElementById(id).checked = false;
			jQuery(row).removeClass('active');
		}else{
			document.getElementById(id).checked = true;
			jQuery(row).addClass('active');
		}
	}
</script>
@endsection