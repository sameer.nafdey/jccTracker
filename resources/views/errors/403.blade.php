@extends('layouts.app')
@section('title', '- Blocks')
@section('pageHeader')

@endsection

@section('content')
<div class="container">
    <h4>Duh! Something went wrong.</h4>
    
    <div class='row' style="margin-bottom: 5%">
        <div class="col-xs-12">
            <h5>Please wait... <span id="countdown">5</span></h5>
        </div>
    </div>
</div>
@endsection
@section('pageJs')

<script type="text/javascript">

(function () {
    var timeLeft = 5,
        cinterval;

    var timeDec = function (){
        timeLeft--;
        document.getElementById('countdown').innerHTML = timeLeft;
        if(timeLeft === 0){
            clearInterval(cinterval);
        }
    };

    cinterval = setInterval(timeDec, 1000);
})();


//Your application has indicated there's an error
window.setTimeout(function(){

    // Move to a new location or you can do something else
    window.location.href = "{{url('/')}}";

}, 5000);

</script>
@endsection
