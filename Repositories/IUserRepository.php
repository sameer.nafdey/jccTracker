<?php
interface IUserRepository {

	public function getAllUsers();

	public function getUserById($id);

	public function createOrUpdate($id = null);
}

//only kept for future improvements
//take a look at following blog to understand
//http://heera.it/laravel-repository-pattern#.V7fXpU1974Z